var bbox = [10, 20, 30, 40]

var steps = [
  {
    id: 'downloadBenchmark',
    script: 'download',
    clean: true, 
    'urls': [
    {
      'path': 'http://ipv4.download.thinkbroadband.com/',
      'fileName': '10MB.zip'
    }],
    size: 10
  },
  {
    id: 'hexbinBenchmark',
    script: 'hexbin',
    clean: true,
    cellWidth: '10'
  }
];

module.exports = {
  steps: steps
}
