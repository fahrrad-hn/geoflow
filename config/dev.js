// USAGE
//
//   mandatory:
//     script:      file name of the script to execute
//     description: description what the script does
//
//   optional:
//     id:          will be used as folder name. This is useful
//                  if a script is reused. If not provided it
//                  will use the script name.
//
//     clean:       clean the output directory before processing.
//                  If clean is false than the script will not be
//                  executed, because it is assumed that the output
//                  files are already available.
//
//     input:       by default the step uses as input the results
//                  from the previous step. It will lookup all files
//                  of the previous steps output. If you specify the
//                  script name or id from another step as input
//                  it will be used for processing

var tools = require('../lib/tools');

var bbox = [ 60, 5.8, 140, 43 ]

var server = {
  'id': 'server',
  'script': 'server',
  'description': 'provide current status and results via a server',
  'clean': true,
  'port': 5555,
}

var hexbin = {
  'script': 'hexbin',
  'id': 'hexbin',
  'description': 'creates a hexbin',
  'bbox': bbox,
  'cellWidth': 50,
  'units': 'kilometers',
  'clean': false
}


var save = {
  'id': 'save',
  'script': 'persist',
  'description': 'save statistics',
  'clean': true
}

steps = [server, hexbin,  save]

var config = {
  scriptPath: './scripts/',
  outputPath: './data/',
  debugLevel: 'info',
  stopOnFailure: true,
  benchmark: false
}

module.exports = {
  description: 'Development Workflow',
  steps: steps,
  config: config
}
