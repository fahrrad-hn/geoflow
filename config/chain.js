// USAGE
//
//   mandatory:
//     script:      file name of the script to execute
//     description: description what the script does
//
//   optional:
//     id:          will be used as folder name. This is useful
//                  if a script is reused. If not provided it 
//                  will use the script name.
//
//     clean:       clean the output directory before processing.
//                  If clean is false than the script will not be
//                  executed, because it is assumed that the output
//                  files are already available.                   
//
//     input:       by default the step uses as input the results
//                  from the previous step. It will lookup all files
//                  of the previous steps output. If you specify the 
//                  script name or id from another step as input 
//                  it will be used for processing

var tools = require('../lib/tools');

var clip = true; // cut to bbox
var clipPoly = true // strategy to stamp raster  before processing

// get bbox from http://bboxfinder.com
//var bbox = '-59070.5354506 736070.237397 140228.314591 606628.716236';

// lower left, upper right
//var bbox = [ 102.5, 8, 112, 18 ]
// vietnam
var bbox = [ 60, 5.8, 140, 43 ]
// eastern part
//var bbox = [ 90, 0, 180, 90 ]
var levels = [8];
var offset = false;

var clean = {
  'script': 'clean',
  'id': 'clean',
  'description': 'remove all files and folders in tmp directory',
  'clean': true
}

var grid = {
  'script': 'grid',
  'id': 'grid',
  'description': 'creates a grid',
  'bbox': [ 90, 0, 180, 90 ],
  'cellSize': [10, 15],
  // type: hex, triangle, point, square
  'type': 'square',
  'inputs': [],
  'units': 'kilometers',
  'clean': false
}

var downloadPopulation = {
  'script': 'download',
  'id': 'downloadPopulation',
  'description': 'Download GHS population grid from [http://ghsl.jrc.ec.europa.eu/ghs_pop.php]',
  'clean': false,
  'inputs': [],
  'urls': [
  {
    'path': 'http://cidportal.jrc.ec.europa.eu/ftp/jrc-opendata/GHSL/GHS_POP_GPW4_GLOBE_R2015A/GHS_POP_GPW42015_GLOBE_R2015A_54009_1k/V1-0/',
    'fileName': 'GHS_POP_GPW42015_GLOBE_R2015A_54009_1k_v1_0.zip'
  }]  
}

var downloadCoastlines = {
  'script': 'download',
  'id': 'downloadCoastlines',
  'description': 'Download coastlines from openstreetmapdata.com',
  'clean': false,
  'inputs': [],
  'urls': [
  { 
    'path': 'http://data.openstreetmapdata.com/',
    'fileName': 'coastlines-generalized-3857.zip'
  }]
}

var reprojectGrid = {
  'script': 'reprojectVector',
  'id': 'reprojectGrid',
  'description': 'Reproject grid',
  'clean': false,
  's_srs': 'EPSG:4326',
  'inputs': ['grid']
}

var reprojectVector = {
  'script': 'reprojectVector',
  'id': 'reprojectVector',
  'description': 'Reproject a vector file from one projection to another',
  'clean': false,
  's_srs': 'EPSG:3857',
  'inputs': ['clipBboxVector']
}

var convertToGeojson = {
  'script': 'convert',
  'id': 'convertToGeojson',
  'description': 'Converts vector files to geojson',
  'inputs': (clip ? ['clipBboxVector'] : ['downloadCoastlines']),
  'levels': levels,
  'clean': false
}

var convertHexbinToShapefile = {
  'script': 'convertToShapefile',
  'id': 'convertHexbinToShapefile',
  'description': 'Converts GeoJSON files to Shapefile',
  'inputs': ['grid'],
  'clean': true
}

var convertGridToSqlite = {
  'script': 'convertToSqlite',
  'id': 'convertHexbinToSqlite',
  'description': 'Converts GeoJSON files to Shapefile',
  'inputs': ['grid'],
  'clean': true
}


// distances: 0.01 = 1000 meters, works fine until 0.03, if the value
//            is larger there will be artifacts

// NULL is returned whenever is not possible deriving a single-sided buffer from the original geometry [a single not-closed LINESTRING is expected as input]
var buffer = {
  'script': 'buffer',
  'id': 'buffer',
  'description': 'Create a buffer from coastlines',
  'inputs': (offset ? ['offsetCurve'] : ['convertToGeojson']),
  'levels': convertToGeojson.levels,
  'distances': [0.5, 0.25, 0.75, 1],
  'clean': false,
  'singleSided': false
}

var rasterize = {
  'script': 'rasterize',
  'id': 'rasterize',
  'description': 'rasterize vector (used later as a clipping mask)',
  'inputs': ['buffer'],
  'resolution': 0.01,
  'clean': false
}

var clipBboxRaster = {
  'script': 'clipBboxRaster',
  'id': 'clipBboxRaster',
  'inputs': ['downloadPopulation'],
  'bbox': bbox,
  'clean': true,
  'description': 'Clips raster data with a bounding box'
}

var clipVector = {
  'script': 'clipVector',
  'id': 'clipVector',
  'inputs': ['grid', 'dissolveBuffer'],
  'description': 'Clip grid cells inside the buffer',
  'clean': true
}

var clipPoly = {
  'script': 'clipPoly',
  'id': 'clipPoly',
  'inputs': (clip ? ['dissolveMollweideBuffer', 'clipBboxRaster'] : ['dissolveMollweideBuffer', 'downloadPopulation']),
  'description': 'Clips raster data with vector polygon',
  'clean': true
}

var dissolveBuffer = {
  'script': 'dissolve',
  'id': 'dissolveBuffer',
  'inputs': ['buffer'],
  'description': 'Dissolve the buffer',
  'clean': false
}

var dissolveMollweideBuffer = {
  'script': 'dissolve',
  'id': 'dissolveMollweideBuffer',
  'inputs': ['bufferMollweide'],
  'description': 'dissolving Mollweide buffer',
  'clean': true
}
  

var vectorize = {
  'script': 'vectorize',
  'id': 'vectorize',
  'description': 'Convert raster to csv data and wrap it with a vrt file. Sort out zero values.',
  'inputs': (clip ? ['clipPoly'] : ['rasterize']),
  'clean': false
}

var vectorizeResample = {
  'script': 'vectorize',
  'id': 'vectorizeResample',
  'description': 'Convert raster to csv data and wrap it with a vrt file. Sort out zero values.',
  'inputs': ['resamplePopulation'],
  'clean': true
}

var csv2geojsonResample = {
  'script': 'csv2geojson',
  'id': 'csv2geojsonResample',
  'description': 'Uses a vrt file and converts the data to geojson.',
  'inputs': ['vectorizeResample'],
  'clean': true
}

var csv2geojson = {
  'script': 'csv2geojson',
  'id': 'csv2geojson',
  'description': 'Uses a vrt file and converts the data to geojson.',
  'inputs': ['vectorize'],
  'clean': false
}

// distances in meters
var lineChunks = {
  'script': 'lineChunks',
  'id': 'lineChunks',
  'description': 'Split line data in line chunks of specified length',
  'inputs': ['convertToGeojson'],
  'distances': [5],
  'clean': false
}

var bufferMollweide = {
  'script': 'buffer',
  'id': 'bufferMollweide',
  'description': 'Create buffer from shapefiles in mollweide projection. Used to create a mask for clipping the input raster',
  'inputs': ['clipBboxVector'],
  'levels': [8],
  'distances': [115000],
  'clean': false,
  'singleSided': false
}


var bufferChunks = {
  'script': 'buffer',
  'id': 'bufferChunks',
  'description': 'Create buffer from line chunks',
  'inputs': ['lineChunks'],
  'levels': buffer.levels,
  'distances': buffer.distances,
  'clean': false,
  'singleSided': false
}

var clipBboxVector = {
  'script': 'clipBboxVector',
  'id': 'clipBboxVector',
  'description': 'Clip a vector file to a bounding box',
  'bbox': clipBboxRaster.bbox,
  'clean': false, 
  'inputs': ['downloadCoastlines']
}

var offsetCurve = {
  'script': 'offsetCurve',
  'id': 'offsetCurve',
  'description': 'Generates a offset curve from vector lines',
  'inputs': ['convert'],
  'distances': [1.0],
  'clean': false
}

var mapshaper = {
  'script': 'mapshaper',
  'id': 'mapshaper',
  'description': 'The mapshaper command line program supports essential map making tasks like simplifying shapes, editing attribute data, clipping, erasing, dissolving, filtering and more.',
  'inputs': ['csv2geojson', 'bufferChunks'],
  'clean': true
}

var crunsh = {
  'script': 'crunsh',
  'id': 'crunsh',
  'description': 'Sum up population data and attaches the attributes to polygons',
  'inputs': ['csv2geojson', 'clipVector'],
  'clean': true
}

var server = {
  'id': 'server',
  'script': 'server',
  'description': 'provide current status and results via a server',
  'clean': true
}

var save = {
  'id': 'save',
  'script': 'persist',
  'description': 'save statistics',
  'inputs': ['downloadCoastlines', 'downloadPopulation', 'grid'],
  'clean': true
}

var resamplePopulation = {
  id: 'resamplePopulation',
  script: 'resample',
  description: 'resample raster to different resolutions',
  inputs: ['clipBboxRaster'],
  resolutions: [1000, 2000, 4000, 8000, 16000, 32000, 64000, 128000],
  method: 'average'
}

//steps = [server, downloadPopulation, downloadCoastlines, grid, convertHexbinToShapefile, clipBboxRaster, clipBboxVector, reprojectVector, convertToGeojson, lineChunks, buffer, bufferChunks, bufferMollweide, dissolveMollweideBuffer, dissolveBuffer, clipVector, clipPoly, rasterize, vectorize, csv2geojson, crunsh]

steps = [server, downloadPopulation, clipBboxRaster, resamplePopulation, vectorizeResample, csv2geojsonResample, save]

if(true) {
  for(var i=0; i < steps.length; i++) {
    var script = steps[i];
    if(script.id != 'downloadPopulation' && script.id != 'downloadCoastlines' && script.id != 'clipBboxRaster') {
      script.clean = true;
    } else {
      script.clean = false;
    }
  };
}

var config = {
  scriptPath: './scripts/',
  outputPath: './data/',
  logLevel: 'info',
  stopOnFailure: true,
  benchmark: false
}

module.exports = {
  description: 'Vectorize a population raster and clip it to buffer zones of coastline to produce a mbtiles file suitable for presentation via https://www.mapbox.com/mapbox-gl-js/api/',
  steps: steps,
  config: config
} 
