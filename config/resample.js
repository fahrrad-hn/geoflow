var tools = require('../lib/tools');

var clip = true; // cut to bbox
var clipPoly = true // strategy to stamp raster  before processing

// lower left, upper right
var bbox = [ 102.5, 8, 112, 18 ]
//var bbox = [ 60, 5.8, 140, 43 ]
// eastern part
//var bbox = [ 0, 0, -180, -90 ]

// empty bbox will process whole image
//var bbox = [];

// Docker
//var gdalImage = 'geodata/gdal';
//var tippecanoeImage = 'jskeates/tippecanoe';

// Variables to share between different steps
var mollweideProj = "+proj=moll +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"

var server = {
  id: 'server',
  script: 'server',
  description: 'provide current status and results via a server',
  port: 8080,
  clean: true
}


var downloadPopulation = {
  script: 'download',
  id: 'downloadPopulation',
  description: 'Download GHS population grid from [http://ghsl.jrc.ec.europa.eu/ghs_pop.php]',
  clean: false,
  inputs: [
    {
      path: 'http://cidportal.jrc.ec.europa.eu/ftp/jrc-opendata/GHSL/GHS_POP_GPW4_GLOBE_R2015A/GHS_POP_GPW42015_GLOBE_R2015A_54009_250/V1-0/',
      fileName: 'GHS_POP_GPW42015_GLOBE_R2015A_54009_250_v1_0.zip',  
    },
    //{
    //  'path': 'http://cidportal.jrc.ec.europa.eu/ftp/jrc-opendata/GHSL/GHS_POP_GPW4_GLOBE_R2015A/GHS_POP_GPW42015_GLOBE_R2015A_54009_1k/V1-0/',
    //  'fileName': 'GHS_POP_GPW42015_GLOBE_R2015A_54009_1k_v1_0.zip'
    //}
  ]  
}

var downloadCoastlines = {
  script: 'download',
  id: 'downloadCoastlines',
  description: 'Download coastlines from openstreetmapdata.com',
  clean: false,
  inputs: [
    {
      path: 'http://data.openstreetmapdata.com/',
      fileName: 'coastlines-generalized-3857.zip',
    },
    {
      path: 'http://data.openstreetmapdata.com/',
      fileName: 'coastlines-split-3857.zip',
      rename: [
        {
          from: 'lines',
          to: 'coastlines_z9'
        }
      ]
    }
  ]
}

var clipBboxVector = {
  script: 'clipBboxVector',
  id: 'clipBboxVector',
  description: 'Clip a vector file to a bounding box',
  bbox: bbox,
  clean: false,
  docker: true,
  inputs: ['downloadCoastlines']
}


var clipBboxRaster = {
  script: 'clipBboxRaster',
  id: 'clipBboxRaster',
  inputs: ['downloadPopulation'],
  bbox: bbox,
  docker: true,
  clean: false,
  description: 'Clips raster data with a bounding box'
}

var vectorize = {
  script: 'vectorize',
  id: 'vectorize',
  description: 'Convert raster to csv data and wrap it with a vrt file. Sort out zero values.',
  inputs: ['resample'],
  clean: true,
  docker: false,
}

var csv2geojson = {
  script: 'csv2geojson',
  id: 'csv2geojson',
  description: 'Uses a vrt file and converts the data to geojson.',
  inputs: ['vectorize'],
  clean: true,
  docker: true,
}

var reprojectVector = {
  script: 'reprojectVector',
  id: 'reprojectVector',
  description: 'Reproject a vector file from one projection to another',
  clean: false,
  s_srs: 'EPSG:3857',
  t_srs: mollweideProj,
  inputs: ['clipBboxVector'],
  docker: true,
}

var convertToGeojson = {
  script: 'convert',
  id: 'convertToGeojson',
  description: 'Converts vector files to geojson',
  inputs: ['reprojectVector'],
  // only process files which have following in file name
  // do not put file extensions because they can switch (e.g. from shp to geojson)
  levels: ['8', '9'],
  clean: false,
  docker: true,
}

// distances in meters
var lineChunks = {
  script: 'lineChunks',
  id: 'lineChunks',
  description: 'Split line data in line chunks of specified length',
  inputs: ['convertToGeojson'],
  distances: [5],
  clean: true
}

// distances: 0.01 = 1000 meters, works fine until 0.03, if the value
//            is larger there will be artifacts

// NULL is returned whenever is not possible deriving a single-sided buffer from the original geometry [a single not-closed LINESTRING is expected as input]
var buffer = {
  script: 'buffer',
  id: 'buffer',
  description: 'Create a buffer from coastlines',
  inputs: ['convertToGeojson'],
  levels: convertToGeojson.levels,
  distances: [0.5, 0.25, 0.75, 1],
  clean: true,
  singleSided: false
}

var bufferChunks = {
  script: 'buffer',
  id: 'bufferChunks',
  description: 'Create buffer from line chunks',
  inputs: ['lineChunks'],
  levels: buffer.levels,
  distances: buffer.distances,
  clean: true,
  singleSided: false
}

var clipPoly = {
  script: 'clipPoly',
  id: 'clipPoly',
  inputs: ['dissolveBuffer', 'clipBboxRaster'],
  description: 'Clips raster data with vector polygon',
  clean: true,
  docker: true,
}

var dissolveBuffer = {
  script: 'dissolve',
  id: 'dissolveBuffer',
  inputs: ['bufferChunks'],
  description: 'Dissolve the buffer',
  clean: false
}

var resample = {
  id: 'resample',
  script: 'resample',
  description: 'resample raster to different resolutions',
  inputs: ['clipBboxRaster'],
  resolutions: [250, 500, 1000, 2000, 4000, 8000, 16000, 32000, 64000],
  method: 'avarage',
  docker: true,
  clean: true
}

var tippecanoe = {
  id: 'tippecanoe',
  script: 'tippecanoe',
  description: 'convert geojson to mbtiles',
  inputs: ['csv2geojson'],
  resolutionsToZoom: [
    {res: 10, minzoom: 19, maxzoom: 19},
    {res: 100, minzoom: 18, maxzoom: 18},
    {res: 250, minzoom: 17, maxzoom: 17},
    {res: 500, minzoom: 15, maxzoom: 16},
    {res: 1000, minzoom: 13, maxzoom: 14},
    {res: 2000, minzoom: 10, maxzoom: 12},
    {res: 4000, minzoom: 8, maxzoom: 9},
    {res: 8000, minzoom: 6, maxzoom: 7},
    {res: 16000, minzoom: 5, maxzoom: 6},
    {res: 32000, minzoom: 3, maxzoom: 4},
    {res: 64000, minzoom: 1, maxzoom: 2},
    {res: 128000, minzoom: 0, maxzoom: 1}],
  clean: true,
  docker: true,
}

var mbtilesMerge = {
  id: 'merge',
  script: 'mbtilesMerge',
  description: 'merge several mbtiles files into one mbtiles',
  inputs: ['tippecanoe'],
  docker: false,
  clean: true
}

var tileserveConfig = {
  id: 'tileserveConfig',
  script: 'tileserveConfig',
  description: 'create tileserver config for mbtiles',
  inputs: ['merge'],
  clean: true
}

var save = {
  id: 'save',
  script: 'persist',
  description: 'save statistics',
  inputs: ['downloadCoastlines', 'downloadPopulation', 'grid'],
  clean: true
}

var tileserve = {
  id: 'tileserve',
  script: 'tileserve',
  description: 'serve mbtiles',
  inputs: ['tileserveConfig'],
  port: 8081,
  clean: true,
  docker: true,
}

//steps = [server, downloadPopulation, clipBboxRaster, downloadCoastlines, resample, vectorize, csv2geojson, tippecanoe, mbtilesMerge, save, tileserveConfig, tileserve]

steps = [server, downloadCoastlines, clipBboxVector, reprojectVector, convertToGeojson, lineChunks, buffer, dissolveBuffer, downloadPopulation, clipBboxRaster, clipPoly]

if(true) {
  for(var i=0; i < steps.length; i++) {
    var script = steps[i];
    if(script.id != 'ownloadPopulation' && script.id != 'downloadCoastlines') {
      script.clean = true;
    } else {
      script.clean = false;
    }
  };
}

var config = {
  scriptPath: './scripts/',
  outputPath: './data/',
  logLevel: 'info',
}

module.exports = {
  description: 'Vectorize a population raster and clip it to buffer zones of coastline to produce a mbtiles file suitable for presentation via https://www.mapbox.com/mapbox-gl-js/api/',
  steps: steps,
  config: config
} 
