'use strict'

const q = require('q');
const exec = require('child_process').exec;

// Custom scripts
const tools = require('../lib/tools');
const log = require('../lib/logger');
const docker = require('../lib/docker');

var scriptPromise = q.defer();
var promises = [];

var start = function(options) {
  var files = tools.getFiles(options.inputs[0], {filter: validateInputs.inputFormats[0]});
  var filesFiltered = [];
 
  if(options.levels != undefined) {
    for(var i=0; i<files.length; i++) {
      for(var j=0; j<options.levels.length; j++) {
        log.warn(files[i]);
        log.warn(options.levels[j]);
         
        if(files[i].indexOf(options.levels[j]) > -1) {
          filesFiltered.push(files[i]);
        }
      }
   }
  } else {
    filesFiltered = files;
  }

  filesFiltered.forEach(function(file) {
    var outputFileName = tools.getFileFromPath(file).replace('.shp', '.geojson');
    var command = 'ogr2ogr -q -f GeoJSON '+tools.getOutputDir(options)+outputFileName+' '+file+' -t_srs EPSG:4326';

    var status = {
      id: outputFileName,
      promise: q.defer(),
      done: false
    };
    promises.push(status);

    if(!options.docker) {
      exec(command, function(error, stdout, stderr) {
        status.done = true;
        checkDone();
      });
    } else {
       docker.execGdal(command, '-d=false').then(function() {
         status.done = true;
         checkDone();
       });
    }
  });
}

var checkDone = function() {
   if(tools.isAllDone(promises)) {
     console.log('all conversions done');
     scriptPromise.resolve(promises);
   }
}

var validateInputs = {
  inputFormats: ['shp'],
  hasElements: [
    {docker: 'boolean'},
    {levels: 'object'}
  ]
}

module.exports = {
  process: function(options) {
    scriptPromise = q.defer();
    start(options);
  
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}
