var q = require('q');
var tools = require('./tools');

var scriptPromise = q.defer();

module.exports = function(options) {
    tools.cleanTmp();
    scriptPromise.resolve('cleaned all files and folders in tmp dir');
    return scriptPromise.promise;
}
