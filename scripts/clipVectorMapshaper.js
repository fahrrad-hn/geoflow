var ogr2ogr = require('ogr2ogr');
var q = require('q');
var fs = require('fs');
var JSONStream = require('JSONStream');
var mapshaper = require('mapshaper');

// custom scripts
var tools = require('./tools');
var scriptPromise = q.defer();
var promises = [];

var clipVector = function(options, target, source) {
 var status = {
   id: target,
   promise: q.defer(),
   done: false
  };
  promises.push(status);
  
   var command = "-i "+target+" -clip "+source+" -o "+options.outputDir+tools.getFileFromPath(source).replace('.geojson', '_clipped.geojson')+" format=geojson";
   console.log(command);

   mapshaper.runCommands(command, function(result) {
     console.log(result.length);
     status.done = true;
     var allDone = tools.isAllDone(promises)
     if(allDone) {
       console.log('clipping done');
       scriptPromise.resolve(promises);
    }
   });
}

var start = function(options) {
  // hexbin
  var target = tools.getFiles(options.inputs[0], '.geojson');
  if(target.length > 1) {
    console.log('there should be only one target (usually hexbin). Detected more.');
  }
  // buffer
  var source = tools.getFiles(options.inputs[1], '.geojson');

  source.forEach(function(polygon) {
    clipVector(options, target[0], polygon);
  });
}

module.exports = function(options) {
  var verify = {
    'numberOfInputs': 2
  };
  verifyOptions = tools.verifyOptions(options, verify);
  console.log(options);

  if(verifyOptions) {
    if(options.clean) {
      start(options);
    } else {
      scriptPromise.resolve('skipped due to option clean=false');
    }
  } else {
    scriptPromise.reject('provided options are not valid. The script expects '+verify.numberOfInputs+' inputs.');
  }

  return scriptPromise.promise;
}

