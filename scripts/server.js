const q = require('q');
const express = require('express');
const http = require('http');

const tools = require('../lib/tools');
const log = require('../lib/logger');

var start = function(options) {
  log.info(options)
  var scriptPromise = q.defer();
  var app = express();
  var server = http.createServer(app);

  app.use('/process.json', function (req, res) {
    log.info('get current process');
    res.send(JSON.stringify(tools.getCurrentProcess(), null, 2));
  });

  app.use('/process.md', function (req, res) {
    var markdown = tools.getMarkdown();
    res.send(markdown);
  });

  app.use('/status', function (req, res) {
    var status = tools.getStatus();
    res.send(status);
  });

  var exit = function() {
    server.close();
  };

  app.use('/server/close', function(req, res) {
    log.info('closing...');
    res.send('closing...');
    req.destroy();
    server.close();
  });

  app.use('/config', express.static('config'));
  app.use('/', express.static(__dirname + "/../frontend"));
  app.use('/graph', function(req, res) {
    res.send(tools.getGraph());
  });

  app.listen(options.port, function() {
    scriptPromise.resolve({
      msg: 'Server started on port '+options.port,
    });
  });

  return scriptPromise.promise;
}

var validateInputs = {
 hasElements: [{port: 'number'}]
}

module.exports = {
  process: function(options) {
    return start(options);
  },
  validateInputs: validateInputs
}
