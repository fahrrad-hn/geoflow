'use strict'

const ogr2ogr = require('ogr2ogr');
const q = require('q');
const fs = require('fs');
const cp = require('child_process');

// custom scripts
const tools = require('../lib/tools');
const log = require('../lib/logger');
const docker = require('../lib/docker');

var scriptPromise = q.defer();
var promises = [];
var containerId;

var resampleRaster = function(raster, resolution, options) {
 var status = {
   id: resolution,
   promise: q.defer(),
   done: false
  };
  promises.push(status);

  var rasterName = raster.split("/")[raster.split("/").length-1];
  
  // gdal >= 2.0, otherwise -tr option is not available
  var command = "gdal_translate -co COMPRESS=LZW -co BIGTIFF=YES -of GTIFF -tr "+resolution+" "+resolution+" -r "+options.method+" "+raster+" "+options.outputDir+resolution+"meter_"+rasterName

  if(!options.docker) {
    log.info('Execute on bare matal')
    cp.exec(command, function(err, stdout, stderr) {
      if(err) {
	log.error(err);
        scriptPromise.reject({msg: err});
      }
      log.info(stdout);
      status.done = true;
      var allDone = tools.isAllDone(promises)
      if(allDone) {
        log.output('resampling ready');
        scriptPromise.resolve(promises);
      }
    });
  } else {
    docker.execGdal(command, " -d=false ").then(function() {
      status.done = true;
      if(tools.isAllDone(promises)) {
        log.info('resampling ready');
        scriptPromise.resolve(promises);
      }
    });
  }
  log.info(command);
}

var start = function(options) {
  var rasters = tools.getFiles(options.inputs[0], { filter: validateInputs.inputFormats[0], recursive: false });  
  rasters.forEach(function(raster) {
    options.resolutions.forEach(function(resolution) {
      resampleRaster(raster, resolution, options);
    });
  });
}

var validateInputs = {
  numberOfInputs: 1,
  inputFormats: ['tif'],
  hasElements: [
    {resolutions:'object'},
    {method:'string'},
    {docker: 'boolean'},
  ]
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}

