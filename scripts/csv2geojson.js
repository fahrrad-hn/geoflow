var fs = require('fs');
var ogr2ogr = require('ogr2ogr');

// custom scripts
var tools = require('../lib/tools');
var log = require('../lib/logger');
var docker = require('../lib/docker');
var q = require('q');

var scriptPromise = q.defer();
var promises = [];
var containerId;

function vectorizeRaster(options) {
  var inputDir = options.inputs[0];
  var vrtFiles = tools.getFiles(inputDir, {filter: '.vrt'});

  vrtFiles.forEach(function(vrtFile) {
    var status = {
      id: vrtFile,
      promise: q.defer(),
      done: false
    };
    promises.push(status);
    var name = tools.getFileFromPath(vrtFile);
    var nameWithoutExtension = name.substr(0, name.lastIndexOf("."));
    var command = "ogr2ogr -s_srs '+proj=moll +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m no_defs' -t_srs EPSG:4326 -q -f GeoJSON -sql 'SELECT cast(field_3 as float) AS population FROM \""+nameWithoutExtension+"\" WHERE cast(field_3 as float) > 0' "+options.outputDir+name+".geojson "+vrtFile+" -progress";

    if(!options.docker) {
      tools.exec(command, function(error, stdout, stderr) {
        status.done = true;
        if(tools.isAllDone(promises)) {
          log.info('all csv2geojson done');
       	  scriptPromise.resolve(promises);
        } 
      });
    } else {
      docker.execGdal(command, " -d=false ").then(function() {
        status.done = true;
        if(tools.isAllDone(promises)) {
          log.info('all csv2geojson done');
          scriptPromise.resolve(promises);
        }
      });
    }
  });
}

  /*var chunkBuffer = ogr2ogr(fc)
    .format('GeoJSON')
    .skipfailures()
    .options(['-dialect', 'sqlite', '-sql', 'select ST_buffer(Geometry,0.01) from OGRGeoJSON'])
    .timeout(1000000)
    .stream()

  var data = '';
  var result = [];
  chunkBuffer.setEncoding('UTF-8');
  chunkBuffer.pipe(fs.createWriteStream('coastlines_z'+level+'_chunks_buffer.geojson'))*/

var start = function(options) {
  vectorizeRaster(options);
}

var validateInputs = {
  numberOfInputs: 1,
  hasElements: [
    {docker: 'boolean'}
  ]
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}
