const turf = require('turf');
const lineChunk = require('turf-line-chunk');
const flatten = require('turf-flatten');
const featureCollection = require('turf-featurecollection');
const ogr2ogr = require('ogr2ogr');
const fs = require('fs');
const q = require('q');
const jsonStream = require('JSONStream');

// custom scripts
const config = require('./config');
const tools = require('../lib/tools');
const log = require('../lib/logger');

var scriptPromise = q.defer();
var promises = [];

var createChunks = function(options) {
  options.distances.forEach(function(distance) {
    var files = tools.getFiles(options.inputs[0], '.geojson');
    
    files.forEach(function(file) {
      var status = {
        id: file,
        promise: q.defer(),
        done: false
      };
      promises.push(status);
      try {
        var coastline = fs.readFileSync(file);
        coastline = JSON.parse(coastline);
        var result = []
        for (var i = 0; i < coastline.features.length; i++) {
          if(coastline.features[i].geometry.type === 'MultiLineString') {
            var flattened = flatten(coastline.features[i]);
            for(j = 0; j < flattened.features.length; j++) {
              result = createLineChunk(flattened.features[j], distance, result);
            }
          } else {
            result = createLineChunk(coastline.features[i], distance, result);
          }
        }
        var outputDir = tools.getOutputDir(options);

        fs.writeFileSync(outputDir+distance+'_'+tools.getFileFromPath(file), JSON.stringify(featureCollection(result)));
      }
      catch(err) {
        scriptPromise.reject('Error: '+err);
      }
      
      status.done = true;
      if(tools.isAllDone(promises)) {
        scriptPromise.resolve(promises);
      }
    });
  });
}

var createLineChunk = function(featureCollection, distance, result) {
  var collection = lineChunk(featureCollection, distance, 'kilometers');
  for(var j = 0; j < collection.features.length; j++) {
    result.push(collection.features[j]);
  }
  return result;
}

var start = function(options) {
  createChunks(options); 
}

validateInputs = {
  hasElements: [
    {distances: 'object'},
  ]
};

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  }
}
