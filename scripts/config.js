var config = {
  buffer: {},
  offset: {}
};

config.levels = [1, 2]
config.buffer.distances = [0.1];
config.buffer.nameTemplate = 'coastlines_z{LEVEL}_buffer_{DISTANCE}.geojson';
config.offset.nameTemplate = 'coastlines_z{LEVEL}_offset_{DISTANCE}.geojson';
config.buffer.chunks = false; // true, false, all

module.exports = config
