var ogr2ogr = require('ogr2ogr');
var fs = require('fs');
var q = require('q');
const readline = require('readline');

// custom scripts
var config = require('./config');
var tools = require('./tools');
var split = require('./lineChunks');

var levels = config.levels;
var outputFileName = config.buffer.nameTemplate; 
var scriptPromise = q.defer();
var bufferPromises = [];

function createBuffers(options, files, inputFormat) {
  if(options.clean) {
    files.forEach(function(file) {
      options.distances.forEach(function(distance) {
  	  var level = tools.getZoomFromFileName(file);
          var postgis = ogr2ogr(file)
            .format('PostgreSQL')
            .destination('PG:host=localhost user=crunsh dbname=crunsh password=wucrunsh')
            .options(["-overwrite", "-nln", ("layer_"+distance).replace('.', '')])
            .exec(function(er) {
              console.log('import finished, proceed with export');
              buffer(options, level, distance);
            });
        });
    });
  } else {
    scriptPromise.resolve('done');
  }
}

function buffer(options, level, distance) {
  var status = {
    id: level+""+distance,
    promise: q.defer(),
    done: false
  };
  bufferPromises.push(status);


  var selectStatement = "\"SELECT ST_Buffer(wkb_geometry, "+distance+", 'endcap=round') FROM "+("layer_"+distance).replace('.', '')+"\"";
  var outputDir = tools.getOutputDir(options);
  var output = outputFileName;
      output = output.replace('{LEVEL}', level);
      output = output.replace('{DISTANCE}', distance);

  var command = "ogr2ogr -f 'GeoJSON' "+outputDir+output+" PG:'host=localhost user=crunsh dbname=crunsh password=wucrunsh' -sql "+selectStatement;
  console.log(command);
  var exec = require('child_process').exec;
  exec(command, function(error, stdout, stderr) {
    status.done = true;
    if(tools.isAllDone(bufferPromises)) {
      // all done. ready to execute next step
      console.log('all done!');
      scriptPromise.resolve(bufferPromises);
    } 
  });
}

var start = function(options, files, inputFormat) {
  if(options.levels) {
    levels = options.levels;
  }

  if(options.distances) {
    distances = options.distances;
  }
  
  createBuffers(options, files, inputFormat);
}

module.exports = function(options) {
  scriptPromise = q.defer();
  bufferPromises = [];
  var verify = {
    'numberOfInputs': 1
  };
  verifyOptions = tools.verifyOptions(options, verify);

  var inputFormat = '';
  // verify all inputs are available
  var files = tools.getFiles(options.inputs[0], '.geojson');
  if(files.length === 0) {
    files = tools.getFiles(options.inputs[0], '.shp');
    if(files.length > 0) {
      inputFormat = 'ESRI Shapefile';
      console.log('Shapefiles found');
    } else {
      console.log('No input files found (tried GoeJSON and Shape)');
    }
  } else {
    inputFormat = 'GeoJSON';
    console.log('GeoJSON found');
  }
  
  if(verifyOptions) {
    start(options, files, inputFormat);
  } else {
    scriptPromise.reject('Options are not valid, please check them in chain.js');
  }

  return scriptPromise.promise;
}
