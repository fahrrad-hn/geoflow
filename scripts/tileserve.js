var ogr2ogr = require('ogr2ogr');
var q = require('q');
var fs = require('fs');

// custom scripts
const tools = require('../lib/tools');
const log = require('../lib/logger');
const docker = require('../lib/docker');

var scriptPromise = q.defer();
var promises = [];
var containerId;

var startTileserver = function(configFile, options) {
 var status = {
   id: configFile,
   promise: q.defer(),
   done: false
  };
  promises.push(status);
  
  var command = " --verbose --config "+configFile;
  log.info(command); 

  if(!options.docker) {
    log.error('not supported without docker'); 
  } else {
    docker.execTileserver(command, " --rm -p "+options.port+":80 ").then(function() {
      status.done = true;
      var allDone = tools.isAllDone(promises)
      if(allDone) {
        scriptPromise.resolve(promises);
      }
    });
    
    /*var docker = cp.exec(command, function(err, stdout, stderr) {
      if(err) {
        log.error(err);
      }
      containerId = stdout.trim().replace(/(\r\n|\n|\r)/gm,"");
      var command = "docker attach "+containerId;
      var container = cp.exec(command, function(err, stdout, stderr) {
        if(err) {
          log.error(err);
        }
        log.warn('attached to container: '+containerId);
        log.warn(stdout);
      });
      container.stdout.on('data', function(data) {
        log.warn(data.toString()); 
      });
      status.done = true;
      var allDone = tools.isAllDone(promises)
      if(allDone) {
        log.output('docker started');
        scriptPromise.resolve(promises);
      }
    });

    process.on('SIGINT', function() {
      var command = "docker stop -t 0 "+containerId;
      log.warn(command);
      log.info('Stop container with id: '+containerId);
      cp.exec(command, function(err, stdout, stderr) {
        if(err) {
          log.error('Failed to stop container with id '+containerId+': '+err);
        } else {
          log.info('Container stopped: '+containerId);
        }
        process.exit();
      });
    });*/
  }
}

var start = function(options) {
  var files = tools.getFiles(options.inputs[0], {filter: validateInputs.inputFormats[0], recursive: false});
  if(files.length > 1 || files.length === 0) {
    var message = 'There should be only one config file, detected '+files.length;
    log.warn(message);
    scriptPromise.reject(message);
  } else {
    startTileserver(files[0], options);
  }
}

var validateInputs = {
  numberOfInputs: 1,
  inputFormats: ['json'],
  hasElements: [
    {docker: 'boolean'}
  ]
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}

