'use strict'

const ogr2ogr = require('ogr2ogr');
const q = require('q');
const fs = require('fs');
const JSONStream = require('JSONStream');

// custom scripts
const tools = require('../lib/tools');
const log = require('../lib/logger');  

var scriptPromise = q.defer();
var promises = [];

var dissolve = function(options, buffer) {
 var status = {
   id: buffer,
   promise: q.defer(),
   done: false
  };
  promises.push(status);
 
  var selectStatement = "";
  var t_srs = "";
  var outputFormat = "";
  var layerName = "";
  var outputFileName = tools.getFileFromPath(buffer);
  if(options.inputFormat === 'ZIP') {
    var layerName = tools.getFileFromPath(buffer).replace('.zip', '');
    selectStatement = "'SELECT ST_Union(geometry) FROM "+layerName+"'";
    buffer = '/vsizip/'+buffer+'/'+layerName+'.shp';
    t_srs = " -t_srs 'proj=moll +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs'"
    outputFormat = "ESRI Shapefile"
    outputFileName = outputFileName.replace('.zip', '_dissolved.zip');
    layerName = outputFileName.replace('.zip', ''); 
  } else {
    selectStatement = "'SELECT ST_Union(geometry) FROM OGRGeoJSON'";
    outputFormat = "GeoJSON";
    outputFileName = outputFileName.replace('.geojson', '_dissolved.geojson');
    layerName = outputFileName.replace('.geojson', '');
  }

  var outputStream = fs.createWriteStream(options.outputDir+outputFileName); 
  var ogrStream = ogr2ogr(buffer)
      .format(outputFormat.replace(/'/g, ""))
      .options(['-dialect', 'sqlite', '-sql', selectStatement.replace(/'/g, ""), '-nln', layerName])
      .timeout(10000000)
      .stream() 

  if(options.inputFormat === 'GeoJSON') {
    // STRANGE: otherwise the stream is closed to early resulting in currupt output data? needs a fix in ogr2ogr node wrapper?
    var streamOutput = ogrStream.pipe(JSONStream.parse('features.*'));
    /*streamOutput.on('data', function(data) {
      //console.log('out: '+JSON.stringify(data));
    });*/
  }
  
  ogrStream.on('error', function(error) {
    //console.log(error);
  });
  outputStream.on('error', function(error) {
    console.log(error);
  });
  /*
  inputStream.on('error', function(error) {
    //console.log(error);
  });
  inputStream.on('close', function() {
    console.log('read input done: '+ outputFileName);
  });
  inputStream.on('finish', function() {
    console.log('finished: '+outputFileName);
  });
  */
  ogrStream.on('close', function() {
    console.log('ogr computation done: '+ outputFileName);
  });
    ogrStream.on('finish', function() {
    console.log('finished: '+outputFileName);
  });
  outputStream.on('finish', function() {
    console.log('finished: '+outputFileName);
  });
  outputStream.on('close', function() {
    console.log('output file closed: '+outputFileName)
    status.done = true;
    if(tools.isAllDone(promises)) {
      // all done. ready to execute next step
      log.info('dissolve ready');
      scriptPromise.resolve('dissolve ready');
    }
  });
  ogrStream.pipe(outputStream);
/*
  var command = "ogr2ogr -f '"+outputFormat+"' "+options.outputDir+outputFileName+" "+buffer+" -dialect sqlite -sql "+selectStatement;
  console.log(command);
 
  var exec = require('child_process').exec;
  exec(command, function(error, stdout, stderr) {
    console.log(error);
    status.done = true;
    var allDone = tools.isAllDone(promises)
    if(allDone) {
      console.log('all buffers dissolved');
      scriptPromise.resolve(promises);
    }
  });
*/
};


var start = function(options) {
  var buffers = tools.getFiles(options.inputs[0], '.geojson');
  if(buffers.length === 0) {
    buffers = tools.getFiles(options.inputs[0], '.zip');
    if(buffers.length === 0) {
      log.warn('could not find any files with extensions '+validateInputs.inputFormats);
      opitons.inputFormat = '';
    } else {
      options.inputFormat = 'ZIP';
    }
  } else {
    options.inputFormat = 'GeoJSON';
  }
  
  if(options.inputFormat !== '') {
    buffers.forEach(function(buffer) {
      dissolve(options, buffer);
    });
  } else {
    log.warn('could not detect input format');
  }
}

var validateInputs = {
  inputFormats: ['geojson', 'zip']
}

module.exports = {
  process: function(options) {
    scriptPromise = q.defer();
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}

