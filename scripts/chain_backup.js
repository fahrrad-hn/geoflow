// USAGE
//
//   mandatory:
//     script:      file name of the script to execute
//     description: description what the script does
//
//   optional:
//     id:          will be used as folder name. This is useful
//                  if a script is reused. If not provided it 
//                  will use the script name.
//
//     clean:       clean the output directory before processing.
//                  If clean is false than the script will not be
//                  executed, because it is assumed that the output
//                  files are already available.                   
//
//     input:       by default the step uses as input the results
//                  from the previous step. It will lookup all files
//                  of the previous steps output. If you specify the 
//                  script name or id from another step as input 
//                  it will be used for processing

var tools = require('./tools');

var clip = false;
//var bbox = '-59070.5354506 736070.237397 140228.314591 606628.716236';

// india
var bbox = '6031677.37078 4323143.7708 9154194.18036 599158.559379';

var levels = [8];
var offset = false;

var clean = {
  'script': 'clean',
  'description': 'remove all files and folders in tmp directory',
}

var downloadPopulation = {
  'script': 'download',
  'id': 'downloadPopulation',
  'description': 'Download population data',
  'clean': false,
  'urls': [
    {
      'path': 'http://cidportal.jrc.ec.europa.eu/ftp/jrc-opendata/GHSL/GHS_POP_GPW4_GLOBE_R2015A/GHS_POP_GPW42015_GLOBE_R2015A_54009_1k/V1-0/',
      'fileName': 'GHS_POP_GPW42015_GLOBE_R2015A_54009_1k_v1_0.zip'
    }
  ] 
}

var downloadCoastlines = {
  'script': 'download',
  'id': 'downloadCoastlines',
  'description': 'Download coastlines from openstreetmapdata.com',
  'clean': false,
  'urls': [
    { 
      'path': 'http://data.openstreetmapdata.com/',
      'fileName': 'coastlines-generalized-3857.zip'
    }
  ]
}

var reprojectVector = {
  'script': 'reprojectVector',
  'description': 'Reproject a vector file from one projection to another',
  'clean': true,
  'inputs': ['downloadCoastlines']
}

var convert = {
  'script': 'convert',
  'id': 'convertToGeojson',
  'description': 'Converts vector files to geojson',
  'inputs': (clip ? ['clipBboxVector'] : ['downloadCoastlines']),
  'levels': levels,
  'clean': true
}

// distances: 0.01 = 1000 meters, works fine until 0.03, if the value
//            is larger there will be artifacts

// NULL is returned whenever is not possible deriving a single-sided buffer from the original geometry [a single not-closed LINESTRING is expected as input]
var buffer = {
  'script': 'buffer',
  'description': 'Create a buffer from coastlines',
  'inputs': (offset ? ['offsetCurve'] : ['convertToGeojson']),
  'levels': convert.levels,
  'distances': [0.03, 0.04, 0.05],
  'clean': true,
  'singleSided': false
}

var clipBboxRaster = {
  'script': 'clipBboxRaster',
  'inputs': ['downloadPopulation'],
  'bbox': bbox,
  'description': 'Clips raster data with a bounding box'
}

var clipPoly = {
  'script': 'clipPoly',
  'inputs': (clip ? ['buffer', 'clipBboxRaster'] : ['buffer', 'downloadPopulation']),
  'description': 'Clips raster data with vector polygon',
  'clean': true
}

var vectorize = {
  'script': 'vectorize',
  'description': 'Convert raster to csv data and wrap it with a vrt file. Sort out zero values.',
}

var csv2geojson = {
  'script': 'csv2geojson',
  'description': 'Uses a vrt file and converts the data to geojson.',
  'inputs': ['vectorize']
}

// distances in meters
var lineChunks = {
  'script': 'lineChunks',
  'description': 'Split line data in line chunks of specified length',
  'inputs': ['convertToGeojson'],
  'distances': [5],
  'clean': true
}

var bufferChunks = {
  'script': 'buffer',
  'id': 'bufferChunks',
  'description': 'Create buffer from line chunks',
  'inputs': ['lineChunks'],
  'levels': buffer.levels,
  'distances': buffer.distances,
  'clean': true,
  'singleSided': false
}

var clipBboxVector = {
  'script': 'clipBboxVector',
  'description': 'Clip a vector file to a bounding box',
  'bbox': clipBboxRaster.bbox, 
  'inputs': ['reprojectVector']
}

var offsetCurve = {
  'script': 'offsetCurve',
  'description': 'Generates a offset curve from vector lines',
  'inputs': ['convert'],
  'distances': [0.03],
  'clean': false
}

var crunsh = {
  'script': 'crunsh',
  'description': 'Sum up population data and attaches the attributes to polygons',
  'inputs': ['csv2geojson', 'bufferChunks']
}

chain = [downloadPopulation, downloadCoastlines, reprojectVector, clipBboxRaster, clipBboxVector, convert, offsetCurve, buffer, clipPoly, vectorize, csv2geojson, lineChunks, bufferChunks, crunsh];

module.exports = chain;
