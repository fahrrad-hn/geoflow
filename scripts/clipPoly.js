'use strict'

const ogr2ogr = require('ogr2ogr');
const q = require('q');
const fs = require('fs');

// custom scripts
const tools = require('../lib/tools');
const log = require('../lib/logger');
const docker = require('../lib/docker');

var scriptPromise = q.defer();
var promises = [];

var clipRaster = function(raster, clip, options) {
 var status = {
   id: clip,
   promise: q.defer(),
   done: false
  };
  promises.push(status);
  
  var rasterName = raster.split("/")[raster.split("/").length-1];
  var clipName = clip.split("/")[clip.split("/").length-1];
 
 // if zip with containing shapefiles
  var command = "";
  var layerName = tools.getFileFromPathWithoutExt(clip);
  
  if(clip.endsWith(".zip")) {
    command = "gdalwarp -co COMPRESS=LZW -of GTIFF -crop_to_cutline -cutline /vsizip/"+clip+"/"+layerName+".shp "+raster+" "+options.outputDir+clipName+rasterName
  } else {
    command = "gdalwarp -co COMPRESS=LZW -of GTIFF -crop_to_cutline -cutline "+clip+" "+raster+" "+options.outputDir+clipName+rasterName
  }
  log.warn(command);
 
  if(!options.docker) {
    tools.exec(command, function(error, stdout, stderr) {
      status.done = true;
      var allDone = tools.isAllDone(promises)
      if(allDone) {
        console.log('clip ready');
        scriptPromise.resolve(promises);
      }
    });
  } else {
    docker.execGdal(command, "-d=false").then(function() {
      status.done = true;
      var allDone = tools.isAllDone(promises)
      if(allDone) {
        console.log('clip ready');
        scriptPromise.resolve(promises);
      }
    });
  }
}

var start = function(options) {
  // vector for clipping mask
  var clips = tools.getFiles(options.inputs[0], '.geojson');
  if(clips.length === 0) {
    console.log('no geojson files found, try zip');
    clips = tools.getFiles(options.inputs[0], '.zip');
  }  

  // rasters to clip
  var rasters = tools.getFiles(options.inputs[1], '.tif');
  rasters.forEach(function(raster) {
    clips.forEach(function(clip) {
      clipRaster(raster, clip, options);
    });
  });
}

var validateInputs = {
  numberOfInputs: 2,
  inputFormats: ['geojson', 'zip', 'tif']  
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  }
}

