// Create buffer from generalized coastline data 
var turf = require('turf');
var lineChunk = require('turf-line-chunk');
var featureCollection = require('turf-featurecollection');
var buffer = require('turf-buffer');
var ogr2ogr = require('ogr2ogr');
var JSONStream = require('JSONStream');
var fs = require('fs');

var tools = require('../lib/tools');

var q = require('q');
var scriptPromise = q.defer();
var promises = [];
var count = 0;

var isInside = function(feature, points, result) {
  count++;
  if(result.length % 1000 === 0 && result.length != 0) {
    console.log('result length is: '+result.length);
  } 
  if(count % 1000 === 0) {
    console.log('polygon count: '+count); 
  }
  for(var j=0; j<points.features.length; j++) {
    var isInside = turf.inside(points.features[j], feature);
    if(isInside) {
      var pop = points.features[j];
      if(feature.properties.population === undefined) {
        feature.properties = pop.properties;
      } else {
        feature.properties.population = parseFloat(feature.properties.population) + parseFloat(pop.properties.population);
      }
    }
  }
  if(feature.properties.population != undefined) {
    feature.properties.population = Math.round(feature.properties.population);
    result.push(feature);
  } else {
    //feature.properties.population = 0;
    //result.push(feature);
  }
  return result
}

var crunsh = function(inputs, options) {
  var begin = Date.now(); 

  // ALTERNATIVE
  // ogr2ogr -f 'GeoJSON' output.shp parks.shp -dialect sqlite \
//-sql "SELECT p.Geometry, p.id id, SUM(t.field1) field1_sum, AVG(t.field2) field2_avg
//FROM parks p, 'trees.shp'.trees t WHERE ST_Contains(p.Geometry, t.Geometry) GROUP BY p.id"

  inputs.forEach(function(input) {
    var polygonFile = input.polygon;
    var pointFile = input.point;
    var result = [];
    try {
      var points = fs.readFileSync(pointFile);
      // globally the points are too large to parse, works only for bbox cropped
      points = JSON.parse(points);
      console.log('Number of points: '+points.features.length);
      var polygon = fs.createReadStream(polygonFile);
      
      //result = [];
      var stream = polygon.pipe(JSONStream.parse('features.*'));
      stream.on('data', function(feature) {
        result = isInside(feature, points, result);
      });
      stream.on('error', function(error) {
        console.log(error);
      });
      stream.on('end', function() {
        fs.writeFileSync(options.outputDir+tools.getFileFromPath(polygonFile), JSON.stringify(featureCollection(result)));
        var executionTime = tools.getExecutionTime(begin, Date.now())
        scriptPromise.resolve({
          executionTime: executionTime,
        });
      });
    } catch(exception) {
      console.log(exception);
    } 
  });
}

var start = function(options) {
  var points = tools.getFiles(options.inputs[0], '.geojson');
  var polygons = tools.getFiles(options.inputs[1], '.geojson');
  var inputs = [];
  console.log(points);
  console.log(polygons); 
  polygons.forEach(function(polygon) {
    points.forEach(function(point) {
      //console.log(tools.getFileFromPath(point));
      //console.log(tools.getFileFromPath(polygon));
      //if(tools.getFileFromPath(point).indexOf(tools.getFileFromPath(polygon)) > -1) {
      //if(tools.getFileFromPath(polygon).indexOf('buffer_0.03') > -1) {
	inputs.push({
          'polygon': polygon,
          'point': point
        });
      //}
    });
  });
  crunsh(inputs, options);
}

module.exports = {
  process: function(options) {
    scriptPromise = q.defer();

    var verify = {
      'numberOfInputs': 2
    };
    var verifyOptions = tools.verifyOptions(options, verify);

    if(verifyOptions) {
      start(options);
    } else {
      scriptPromise.reject('options are not valid, please check them in chain.js');
    }

    return scriptPromise.promise;
  }
}
