var ogr2ogr = require('ogr2ogr');
var q = require('q');
var fs = require('fs');
var sqlite = require('sqlite3');
const log = require('../lib/logger');

// custom scripts
var tools = require('../lib/tools');
var scriptPromise = q.defer();
var promises = [];
var dstMeta = {};
var dstDb;

var addSat = function(style) {
  style.sources.mapbox = {
    "url": "mapbox://mapbox.satellite",
    "type": "raster",
    "tileSize": 256
  };
  return style;
}

var createStyle = function(files, options) {
 var status = {
   id: files,
   promise: q.defer(),
   done: false
  };
  promises.push(status);

  var stylesDir = "/styles";
  
  tools.createDir(options.outputDir+"/glyphs");
  tools.createDir(options.outputDir+"/sprites");
  tools.createDir(options.outputDir+stylesDir);

  var writeStyle = function(layers, name) {
    tools.copyDir('./assets/tileserver', options.outputDir);
    tools.copyFile('./assets/tileserver/sprites/template.json', options.outputDir+'sprites/'+layerName+'.json');
    tools.copyFile('./assets/tileserver/sprites/template.png', options.outputDir+'sprites/'+layerName+'.png');
    tools.copyFile('./assets/tileserver/sprites/template@2x.json', options.outputDir+'sprites/'+layerName+'@2x.json');
    tools.copyFile('./assets/tileserver/sprites/template@2x.png', options.outputDir+'sprites/'+layerName+'@2x.png');
    
    var output = options.outputDir+stylesDir+"/"+layerName+"_style.json";
    var layerNameWithoutDot = layerName.replace(/\./g, '');
    style.name = layerNameWithoutDot;
    style.sprite = layerName;
    style.glyphs = 'mapbox://fonts/mapbox/{fontstack}/{range}.pbf';
    style.sources.local.url = 'mbtiles://{'+layerNameWithoutDot+'}';
    style = addSat(style);
    style.layers = layers 
    style.id = layerNameWithoutDot;
    fs.writeFile(output, JSON.stringify(style, null, 2), 'utf8', function() {
      log.output('style written: '+output);
    });
  }

  for(var i=0; i<files.length; i++) {
    var layerName = tools.getFileFromPathWithoutExt(files[i]);
    var layerNameWithoutDot = layerName.replace(/\./g, '');
    var mbtiles = new sqlite.Database(files[i]);
    mbtiles.all("SELECT name, value FROM metadata", function(err, entries) {
      log.output('received mbtiles result');
      var layers = [];
      var circleColors = ["hsl(52, 100%, 50%)", "hsl(96, 38%, 62%)", "hsl(182, 100%, 50%)", "hsl(217, 100%, 50%)", "hsl(63, 100%, 29%)", "hsl(20, 100%, 50%)", "hsl(43, 100%, 50%)", "hsl(0, 100%, 50%)"];
      entries.forEach(function(entry) {
        if(entry.name === 'json') {
          var vectorLayers = JSON.parse(entry.value).vector_layers;
          log.output(JSON.stringify(vectorLayers, null, 2));
          vectorLayers.forEach(function(layer) {
            var layerStyle = {};
            layerStyle.id = layer.id.replace(/\./g, '');
            layerStyle.type = 'circle';
            layerStyle.source = "local";
            layerStyle['source-layer'] = layer.id.replace(/\./g, '');
            layerStyle.layout = {"visibility": "visible"},
            layerStyle.filter = [
                "==",
                "$type",
                "Point"
            ],
            layerStyle.paint = {
              "circle-color": circleColors[Math.floor(Math.random()*circleColors.length)]
            }
            layers.push(layerStyle);
          });
        }
      });
      /*layers.unshift({
        "id": "background",
        "type": "background",
        "paint": {
          "background-color": "rgb(4,7,14)"
        }
      });*/
      layers.unshift({
        "id": "satellite",
        "type": "raster",
        "source": "mapbox",
        "source-layer": "mapbox_satellite_full"
      });

      writeStyle(layers, layerName);
      //log.warn(JSON.stringify(layerStyles, null, 2));
    });
  }

  for(var i=0; i<files.length; i++) {
    var layerName = tools.getFileFromPathWithoutExt(files[i]);
    config.data[layerName.replace(/\./g, '')] = {mbtiles: tools.getFileFromPath(files[i])};
    config.styles[layerName.replace(/\./g, '')] = {
      "style": layerName+"_style.json",
      "tilejson": {
        "type": "overlay"
      },
      serve_data: true
    }
    config.options.paths.mbtiles = "../../"+options.inputs[0]; 
  }  

  var output = options.outputDir+'tileserveConfig.json';
  fs.writeFile(output, JSON.stringify(config, null, 2), 'utf8', function() {
    status.done = true;
    var allDone = tools.isAllDone(promises)
    if(allDone) {
      log.output('config written: '+output);
      scriptPromise.resolve(promises);
    };
  });
}

var style = { 
    "version": 8,
    "name": "Population",
    "metadata": {
        "mapbox:autocomposite": false,
        "mapbox:type": "default"
    },
    "zoom": 9.204977034527152,
    "bearing": 0,
    "pitch": 0,
    "sources": {
        "local": {
            "url": "",
            "type": "vector"
        }
    },
    "sprite": "ciwejwx4w005f2pmqpe78g73c",
    "glyphs": "{fontstack}/{range}.pbf",
    "layers": [],
    "created": "2016-12-07T06:26:03.591Z",
    "id": "ciwejwx4w005f2pmqpe78g73c",
    "modified": "2016-12-07T11:46:31.861Z",
    "owner": "xyz",
    "draft": false
}

var config = {
  "options": {
    "paths": {
      "fonts": "glyphs",
      "sprites": "sprites",
      "styles": "styles",
      "mbtiles": "mbtiles"
    },
    //"frontPage": "../../frontend/tileserver"
  },
  "styles": {},
  "data": {},
}

var start = function(options) {
  var mbtiles = tools.getFiles(options.inputs[0], validateInputs.inputFormats[0]);
  createStyle(mbtiles, options);
}

var validateInputs = {
  numberOfInputs: 1,
  inputFormats: ['mbtiles']
}

module.exports = {
  process: function(options) {
    log.warn('start styling');
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}

