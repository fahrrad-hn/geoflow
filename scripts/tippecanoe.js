const ogr2ogr = require('ogr2ogr');
const q = require('q');
const fs = require('fs');

// custom scripts
const log = require('../lib/logger');
const tools = require('../lib/tools');
const docker = require('../lib/docker');

var scriptPromise = q.defer();
var promises = [];
var containerId;

var createVectorTiles = function(file, minzoom, maxzoom, options) {
 var status = {
   id: file,
   promise: q.defer(),
   done: false
  };
  promises.push(status);
 
  var command = "--output="+tools.getOutputDir(options)+tools.getFileFromPathWithoutExt(file)+".mbtiles "+file+" -Z "+minzoom+" -z "+maxzoom+" -f --no-feature-limit --no-tile-size-limit -B"+minzoom;
  if(!options.docker){
    log.info('Execute on bare metal');
    log.warn('create vector tiles for '+file+', minzoom: '+minzoom+', maxzoom: '+maxzoom);
    tools.exec(command).then(function(res) {
    console.log(res);
    /*status.done = true;
    var allDone = tools.isAllDone(promises)
    if(allDone) {
      log.output('all mbtiles ready');
      scriptPromise.resolve(promises);
    }*/
    })
  } else {
    log.info('Execute with docker');
    docker.execTippecanoe(command, " -d=false --entrypoint='tippecanoe'").then(function() {
      status.done = true;
      var allDone = tools.isAllDone(promises)
      if(allDone) {
        log.output('all mbtiles ready');
        scriptPromise.resolve(promises);
      }
    });
  }
}

var start = function(options) {
  var geojsonFiles = tools.getFiles(options.inputs[0], {filter: validateInputs.inputFormats[0]});  
  geojsonFiles.forEach(function(file) {
    options.resolutionsToZoom.forEach(function(resToZoom) {
      if(file.indexOf('/'+resToZoom.res+'meter') !== -1) {
        createVectorTiles(file, resToZoom.minzoom, resToZoom.maxzoom, options);
      }
    });
  });
}

var validateInputs = {
  numberOfInputs: 1,
  inputFormats: ['geojson'],
  hasElements: [
    {docker: 'boolean'}
  ]
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}

