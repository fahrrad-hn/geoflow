var fs = require('fs');
var ogr2ogr = require('ogr2ogr');

// custom scripts
var tools = require('../lib/tools');
var log = require('../lib/logger');
var docker = require('../lib/docker');
var q = require('q');
var exec = require('child_process').exec;

var scriptPromise = q.defer();
var promises = [];
var containerId;

function vectorizeRaster(options) {
  var rasters = tools.getFiles(options.inputs[0], {filter: validateInputs.inputFormat[0], recursive: false});

  rasters.forEach(function(raster) {
    var status = {
      id: raster,
      promise: q.defer(),
      done: false
    };
    promises.push(status);

    var rasterName = raster.split("/")[raster.split("/").length-1];
    var name = tools.getFileFromPath(raster);
    
    // replaces white spaces with commata and deletes all lines where the line ends with 0 (*$ pins end of line)
    var command = "gdal_translate -a_nodata 0 -of XYZ "+raster+" /vsistdout/ | sed 's/\ /,/g' | sed '/,0*$/d' > "+options.outputDir+rasterName+".csv";
    log.info(command);
    writeVrt(options.outputDir, rasterName);
    if(!options.docker) {
      exec(command, function(error, stdout, stderr) {
        status.done = true;
        if(tools.isAllDone(promises)) {
          scriptPromise.resolve(promises);
        } 
      });
    } else {
       docker.execGdal(command, "-d=false").then(function() {
         status.done = true;
         if(tools.isAllDone(promises)) {
           log.info('all vectorize done');
           scriptPromise.resolve(promises);
         }
       }); 
    }
  });
}

var writeVrt = function(dir, fileName) {
  var xml = `<OGRVRTDataSource>
<OGRVRTLayer name="{LAYERNAME}">
<SrcDataSource relativeToVRT="1">{CSV}</SrcDataSource>
<GeometryType>wkbPoint</GeometryType>
<LayerSRS>EPSG:4326</LayerSRS>
<GeometryField encoding="PointFromColumns" x="field_1" y="field_2" z="field_3"/>
</OGRVRTLayer>
</OGRVRTDataSource>
`;
  xml = xml.replace('{CSV}', fileName+'.csv');
  xml = xml.replace('{LAYERNAME}', fileName);
  fs.writeFileSync(dir+fileName+'.vrt', xml);
}

var start = function(options) {
  vectorizeRaster(options);
}

var validateInputs = {
  numberOfInputs: 1,
  inputFormat: ['tif'],
  hasElements: [
    {docker: 'boolean'}
  ]
}

module.exports = {
  process: function(options) {
    start(options);

    return scriptPromise.promise;
  },
  validateInputs: {
    numberOfInputs: 1,
    inputFormat: ['tif']
  }
}
