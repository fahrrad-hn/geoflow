var turf = require('turf');
var lineChunk = require('turf-line-chunk');
var flatten = require('turf-flatten');
var featureCollection = require('turf-featurecollection');
var ogr2ogr = require('ogr2ogr');
var fs = require('fs');
var q = require('q');
var jsonStream = require('JSONStream');
var mapshaper = require('mapshaper');

// custom scripts
var config = require('./config');
var tools = require('./tools');
var scriptPromise = q.defer();
var promises = [];

var createChunks = function(options) {
  distances.forEach(function(distance) {
    var files = tools.getFiles(options.inputs[0], '.geojson');
    
    files.forEach(function(file) {
      var status = {
        id: file,
        promise: q.defer(),
        done: false
      };
      promises.push(status);
      
      try {
        var coastline = fs.readFileSync(file);
        coastline = JSON.parse(coastline);
        var result = []
        console.time("turf.line-chunk: "+distance+file);
        for (var i = 0; i < coastline.features.length; i++) {
          if(coastline.features[i].geometry.type === 'MultiLineString') {
            var flattened = flatten(coastline.features[i]);
            for(j = 0; j < flattened.features.length; j++) {
              result = createLineChunk(flattened.features[j], distance, result);
            }
          } else {
            result = createLineChunk(coastline.features[i], distance, result);
          }
        }
        console.timeEnd("turf.line-chunk: "+distance+file);
        var outputDir = tools.getOutputDir(options);

        fs.writeFileSync(outputDir+distance+'_'+tools.getFileFromPath(file), JSON.stringify(featureCollection(result)));
      }
      catch(err) {
        scriptPromise.reject('Error: '+err);
      }
      
      status.done = true;
      console.log(status);
      if(tools.isAllDone(promises)) {
        console.log('script finished');
        scriptPromise.resolve(promises);
      }
    });
  });
}

var start = function(options) {
  var points = tools.getFiles(options.inputs[0], '.geojson');
  var polygons = tools.getFiles(options.inputs[1], '.geojson');

  var i = 0;
  points.forEach(function(point) {
    polygons.forEach(function(polygon) {
      //mapshaper.runCommands('-i shapefiles/*.shp -o geojson/ format=geojson');
      var command = 'ogr2ogr -clipsrc ' + point + ' out.geojson '+polygon;
      //console.log(cfsadommand);
      if(i===0) {
        //var command = "-i "+polygon+" -clip "+point+" -o ./test.geojson format=geojson";
        console.log(command);

        mapshaper.runCommands(command, function(result) {
          console.log(result);
        });
        i++;
      }
    });
  });
}

module.exports = function(options) {
  var verify = {
    'numberOfInputs': 2,
  };
  var verifyOptions = tools.verifyOptions(options, verify);

  if(verifyOptions) {
    start(options);
  } else {
    scriptPromise.reject('Options are not valid, please check them in chain.js');
  }

  return scriptPromise.promise;
}
