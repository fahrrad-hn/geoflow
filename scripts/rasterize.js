var ogr2ogr = require('ogr2ogr');
var q = require('q');
var fs = require('fs');

// custom scripts
var tools = require('../lib/tools');
var scriptPromise = q.defer();
var promises = [];

var rasterize = function(polygon, options) {
 var status = {
   id: polygon,
   promise: q.defer(),
   done: false
  };
  promises.push(status);

  console.log(polygon);
  //console.log(options);
  
  var command = "gdal_rasterize -te -180 -90 180 90 -burn 1 -tr 1 1 -ot Byte "+polygon+" "+options.outputDir+tools.getFileFromPath(polygon)+'_rasterize.tif';
  console.log(command);
  var exec = require('child_process').exec;
  exec(command, function(error, stdout, stderr) {
    status.done = true;
    var allDone = tools.isAllDone(promises)
    if(allDone) {
      console.log('all polygons rasterized');
      scriptPromise.resolve(promises);
    }
  });
}

var start = function(options) {
  var polygons = tools.getFiles(options.inputs[0], '.geojson');  
  
  polygons.forEach(function(polygon) {
    rasterize(polygon, options);
  });
}

module.exports = {
  process: function(options) {
    var verify = {
      'numberOfInputs': 1
    };
    console.log(options);
    verifyOptions = tools.verifyOptions(options, verify);

    if(verifyOptions) {
      if(options.clean) {
        start(options);
      } else {
        scriptPromise.resolve('skipped due to option clean=false');
      }
    } else {
      scriptPromise.reject('provided options are not valid. The script expects '+verify.numberOfInputs+' inputs.');
    }

    return scriptPromise.promise;
  }
}

