var turf = require('turf');
var lineChunk = require('turf-line-chunk');
var featureCollection = require('turf-featurecollection');
var buffer = require('turf-buffer');
var ogr2ogr = require('ogr2ogr');
var fs = require('fs');
var q = require('q');
const readline = require('readline');

// custom scripts
var config = require('./config');
var tools = require('./tools');
var split = require('./lineChunks');

var levels = config.levels;
var distances = config.buffer.distances;
var outputFileName = config.offset.nameTemplate; 
var scriptPromise = q.defer();
var bufferPromises = [];

function offsetCurve(options) {
  if(options.clean) { 
    var files = tools.getFiles(options.inputs[0], '.geojson');
      files.forEach(function(file) {
        console.log(file);
        distances.forEach(function(distance) {

          var status = {
            id: file,
            promise: q.defer(),
            done: false 
          };
          bufferPromises.push(status);

          var coastline = fs.readFileSync(file);
          coastline = JSON.parse(coastline);
    
          var offset = ogr2ogr(coastline)
            .format('GeoJSON')
            .skipfailures()
            .options(['-dialect', 'sqlite', '-sql', 'select ST_OffsetCurve(Geometry,'+distance+') from OGRGeoJSON'])
            .timeout(10000000)
            .stream()

          var level = tools.getZoomFromFileName(file);
          var output = outputFileName;
          output = output.replace('{LEVEL}', level);
          output = output.replace('{DISTANCE}', distance);

          offset.pipe(fs.createWriteStream(options.outputDir+output));
          offset.on('end', function() {
            status.done = true;
            if(tools.isAllDone(bufferPromises)) {
              // all done. ready to execute next step
              scriptPromise.resolve(bufferPromises);
            }
          });
        });
    });
  } else {
    scriptPromise.resolve('done');
  }
}

var start = function(options) {
  if(options.distances) {
    distances = options.distances;
  }

  offsetCurve(options);
}

module.exports = function(options) {
  var verify = {
    'numberOfInputs': 1
  };
  verifyOptions = tools.verifyOptions(options, verify);

  if(verifyOptions) {
    start(options);
  } else {
    scriptPromise.reject('Options are not valid, please check them in chain.js');
  }

  return scriptPromise.promise;
}
