const q = require('q');

// custom scripts
const log = require('../lib/logger');
const tools = require('../lib/tools');
const docker = require('../lib/docker');

var scriptPromise = q.defer();
var promises = [];

var start = function(options) {
  var inputFiles = tools.getFiles(options.inputs[0], {filter: validateInputs.inputFormats[0], recursive: true});
  var outputFormat = "ESRI Shapefile";

  if(inputFiles.length === 0) {
    inputFiles = tools.getFiles(options.inputs[0], {filter: validateInputs.inputFormats[1], recursive: true});
    outputFormat = "GeoJSON";
  }
   
  inputFiles.forEach(function(file) {
    var status = {
      id: file,
      promise: q.defer(),
      done: false
    };
    promises.push(status);

    var command = 'ogr2ogr -f "'+outputFormat+'" '+tools.getOutputDir(options)+tools.getFileFromPath(file)+' '+file+' -s_srs '+options.s_srs+' -t_srs "'+options.t_srs+'"';
    if(!options.docker) {
      tools.execCommand(command);
      scriptPromise.resolve('reprojected');
    } else {
      docker.execGdal(command, "-d=false").then(function() {
        status.done = true;
        var allDone = tools.isAllDone(promises)
        if(allDone) {
          log.output('reprojection done');
          scriptPromise.resolve(promises);        
        }
      });
    };
  });
}

var validateInputs = {
  inputFormats: ['shp', 'geojson'],
  hasElements: [
    {docker: 'boolean'},
    {s_srs: 'string'},
    {t_srs: 'string'},
  ]
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}
