var q = require('q');
var tools = require('./tools');
var fs = require('fs');
var ogr2ogr = require('ogr2ogr');

var scriptPromise = q.defer();
var promises = [];


var start = function(options) {
  var files = tools.getFiles(options.inputs[0], '.geojson');
  files.forEach(function(file) {
    var outputFileName = tools.getFileFromPath(file).replace('.shp', '.geojson');

    var status = {
      id: outputFileName,
      promise: q.defer(),
      done: false
    };
    promises.push(status);

    console.log(options.outputDir+tools.getFileFromPath(file).replace('.geojson', '_clipped.sqlite'));
    var outputStream = fs.createWriteStream(options.outputDir+tools.getFileFromPath(file).replace('.geojson', '_clipped.sqlite'));
    var output = ogr2ogr(file)
       .format('SQLite')
       //.skipfailures()
       .options(['-dsco', 'SPATIALITE=YES', '-lco', "SPATIAL_INDEX=YES", '-nln', tools.getFileFromPathWithoutExt(file)])
       .timeout(10000000)
       .stream()

   output.pipe(outputStream);
   output.on('end', function() {
     status.done = true;
     if(tools.isAllDone(promises)) {
       // all done. ready to execute next step
       scriptPromise.resolve(promises);
      }
   });
  });
/**
    var command = 'ogr2ogr -q -f GeoJSON '+tools.getOutputDir(options)+outputFileName+' '+file+' -t_srs EPSG:4326';
    var result = tools.execCommand(command);
    status.done = true;
    
    var allDone = tools.isAllDone(promises)
    if(allDone) {
     scriptPromise.resolve(promises);
    }
  });
*/
}

module.exports = function(options) {
  var verify = {
    numberOfInputs: 1
  }

  if(tools.verifyOptions(options, verify)) {
    if(options.clean) {
      start(options);
    } else {
      scriptPromise.resolve('nothing to do here, clean is deactivated');
    }
  } else {
    scriptPromise.reject('Options are not valid, please check them in chain.js');
  }
  
  return scriptPromise.promise;
}
