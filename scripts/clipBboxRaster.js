const ogr2ogr = require('ogr2ogr');
const q = require('q');
const fs = require('fs');

// custom scripts
const tools = require('../lib/tools');
const log = require('../lib/logger');
const docker = require('../lib/docker');
const proj4 = require('proj4');

var scriptPromise = q.defer();
var promises = [];
var containerId;

var startDocker = function(raster, options) {
 var status = {
   id: raster,
   promise: q.defer(),
   done: false
  };
  promises.push(status);
  
  tools.convertBboxToMollweide('EPSG:4326', options.bbox).then(function(bbox) {
    var command = 'gdalwarp -co compress=LZW -co BIGTIFF=YES -te '+bbox.join(" ")+' ' +raster+ ' '+tools.getOutputDir(options)+tools.getFileFromPath(raster)+' -s_srs "+proj=moll +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m no_defs"';
    docker.execGdal(command, " -d ").then(function() {
      scriptPromise.resolve(promises);
    });
  });
}

var start = function(options) {
  var inputFiles = tools.getFiles(options.inputs[0], { filter: validateInputs.inputFormats[0], recursive: true });
  if(options.bbox.length === 0) {
    inputFiles.forEach(function(raster) {
      log.info('bbox is configured as empty array - will not clip raster');
      tools.copyFile(raster, tools.getOutputDir(options)+tools.getFileFromPath(raster));
    });
    scriptPromise.resolve({msg: 'Clipped raster files'});
  } else {
    inputFiles.forEach(function(raster) {
      startDocker(raster, options);
    }); 
  }
}

var validateInputs = {
  numberOfInputs: 1,
  inputFormats: ['tif'],
  // check if all needed options are available
  hasElements: [{
    docker: 'boolean',
    bbox: 'object'
  }]
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}

