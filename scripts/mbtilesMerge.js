var ogr2ogr = require('ogr2ogr');
var q = require('q');
var fs = require('fs');
var sqlite = require('sqlite3');
const log = require('../lib/logger');

// custom scripts
var tools = require('../lib/tools');
var scriptPromise = q.defer();
var dstDb;
var dstMeta = {};

var execCommand = function(status) {
  //log.warn('execCommand');
  var promise = status.promise;
  status.processing = true;
  var exec = require('child_process').exec;
  var cmd = "sh ./lib/patch.sh "+status.src+" "+status.dst;
  log.warn('execute command: '+cmd);
  exec(cmd, function(err, stdout, stderr) {
    if(err) {
      log.error(err);
      //promise.reject(err);
    } else {
      status.done = true; 
      promise.resolve('done: '+status.id);
    }
  })
  return promise.promise;
}

var merge = function(promises, options) {
  //log.warn('merge');
  var allDone = tools.isAllDone(promises);
  if(allDone) {
    log.output('all mbtiles merged, ubpdate metadata table');
    scriptPromise.resolve(promises);
  } else {
    log.warn('there are still promises to process');
    for(var i=0; i<promises.length; i++) {
      var status = promises[i]; 
      if(!status.processing && !status.done) {
        log.warn('proceed with index: '+i);
        execCommand(status).then(function() {
          log.warn('command executed, update metadata');
          // manually update metadata table
          updateMetadataTable(status.src, status.dst).then(function() {
            // proceed with next
            log.warn('metadata updated, proceed with next');
            merge(promises, options);
          });
        });
        break;
      }
    }
  }
}  


var updateMetadataTable = function(src, dst) {
  var promise = q.defer();
  log.debug('src: '+src);
  log.debug('dst: '+dst);
  var srcDb = new sqlite.Database(src);
  var dstDb = new sqlite.Database(dst);
  log.debug('update metadata table with data from: '+src);

  var queryResults = q.defer();
  dstDb.all("SELECT name, value FROM metadata", function(err, dstResults) {
     if(err) {
       log.error(err);
     } else {
       dstResults.forEach(function(dstEntry) {
         if(dstEntry.name === 'json') {
           dstEntry.value = JSON.parse(dstEntry.value);
         };
         if(dstEntry.name === 'center' || dstEntry.name === 'bounds') {
           dstEntry.value = dstEntry.value.split(",").map(Number);
         }
         dstMeta[dstEntry.name] = dstEntry.value;
        });

        srcDb.all("SELECT * FROM metadata", function(err, srcResults) {
          if(err) {
            log.error(err);
          } else {
            //log.output(JSON.stringify(srcResults, null, 2));
            for(var i=0; i<srcResults.length; i++) {
              var srcEntry = srcResults[i];
              switch(srcEntry.name) {
                case 'minzoom':
                  if(parseInt(srcEntry.value) < parseInt(dstMeta.minzoom)) {
                    log.debug('updated minzoom from '+dstMeta.minzoom+' to '+srcEntry.value);
                    dstMeta.minzoom = parseInt(srcEntry.value);
                  };
                  break;
                case 'maxzoom':
                  if(parseInt(srcEntry.value) > parseInt(dstMeta.maxzoom)) {
                    log.debug('updated maxzoom from '+dstMeta.maxzoom+' to '+srcEntry.value);
                    dstMeta.maxzoom = parseInt(srcEntry.value);
                  }
                  break;
                case 'json':
                  var layer = JSON.parse(srcEntry.value).vector_layers[0];
                  log.debug('pushed new layer: '+layer);
                  dstMeta.json.vector_layers.push(layer);
                  break;
              }
              if(i === srcResults.length-1) {
                queryResults.resolve();
              }
            }
          };
       });
     };
   });

  var queryPromise = queryResults.promise;  
  queryPromise.then(function() {
    var updateCommand = "";
    log.debug('database query finished');
    log.debug(JSON.stringify(dstMeta, null, 2));
    Object.keys(dstMeta).forEach(function(key) {
      var value = dstMeta[key];
      if(key === 'json') {
        value = JSON.stringify(value);
      }
      updateCommand += "UPDATE metadata SET value = '"+value+"' WHERE name = '"+key+"';";
    });

    var execCallback = function(err) {
      log.debug('all done, close db connection');
      dstDb.close();
      promise.resolve('done');
    }
    log.debug(updateCommand);
    dstDb.exec(updateCommand, execCallback);
  });

  return promise.promise;
}

var start = function(options) {
  var mbtiles = tools.getFiles(options.inputs[0], validateInputs.inputFormats[0]);
  var lastFile = mbtiles[mbtiles.length-1];
  var promises = [];
  fs.readFile(lastFile, function(err, data){
    if(err){
      throw err;
    } else {
      var dstFile = tools.getOutputDir(options)+tools.getFileFromPath(lastFile);
      fs.writeFile(dstFile, data, function(err) {
        if(err){
          throw err;
        } else {
          dstDb = new sqlite.Database(dstFile);
          log.debug('read destination metadata');
          dstDb.all("SELECT name, value FROM metadata", function(err, result) {
            if(err) {
              log.error(err);
            } else {
              result.forEach(function(entry) {
                if(entry.name === 'json') {
                  entry.value = JSON.parse(entry.value);
                };
                if(entry.name === 'center' || entry.name === 'bounds') {
                  entry.value = entry.value.split(",").map(Number);
                }
                dstMeta[entry.name] = entry.value;
              });
              //dstMeta.vector_layers = JSON.parse(result.value).vector_layers;
              for(var i=0; i<mbtiles.length-1; i++) {
                var srcFile = mbtiles[i];
                var status = {
                  id: srcFile,
                  src: srcFile,
                  dst: dstFile,
                  promise: q.defer(),
                  done: false,
                  processing: false,
                };
                promises.push(status); 
              }
              merge(promises, options);
            }
          });
        }
      });
    }
  });
}

var validateInputs = {
  numberOfInputs: 1,
  inputFormats: ['mbtiles']
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}

