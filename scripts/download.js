'use strict'

const q = require('q');
const  http = require('http');
const fs = require('fs');
const unzip = require('unzip');

const log = require('../lib/logger');
const tools = require('../lib/tools');

var scriptPromise = q.defer();
var promises = [];

var start = function(options) {  
  var inputs = options.inputs;

  inputs.forEach(function(input) {
    var fileName = input.fileName;
    var path = input.path;
    var renameOptions = input.rename;

    var status = {
      id: fileName,
      promise: q.defer(),
      done: false
    };
    promises.push(status);

    if(!fs.existsSync(tools.getOutputDir(options)+fileName)) {
      var file = fs.createWriteStream(tools.getOutputDir(options)+fileName);
      var url = path+fileName;

      log.info('Download file: '+url);
      var request = http.get(url, function(response) {
        response.pipe(file);
        file.on('finish', function() {
          file.close();
          var outputDir = tools.getOutputDir(options);
          var unzip = tools.unzip(outputDir+fileName, outputDir);
          unzip.on('close', function(results) {
            status.done = true;
            if(renameOptions) {
              log.info('Rename files: '+JSON.stringify(renameOptions));
              tools.renameFiles(outputDir+fileName.replace('.zip','') , renameOptions);
            }
            checkAllDone();
          });
        });
      }).on('error', function(err) {
        log.error(err);
        fs.unlink(file)
      });
      checkAllDone();
    } else {
      var unzip = tools.unzip(tools.getOutputDir(options)+fileName, tools.getOutputDir(options));
      unzip.on('close', function(results) {
        status.done = true;
        log.error('rename from '+rename.from+' to '+rename.to);
        checkAllDone();
      });
    }
  });
}

var checkAllDone = function() {
  var allDone = tools.isAllDone(promises)
  if(allDone) {
    scriptPromise.resolve(promises);
  }
}

var validateInputs = {
  hasElements: [{inputs: 'object'}]
}

module.exports = {
  description: 'Download data from an url and extract the zip to a folder',
  process: function(options) {
    // important to make it reusable: require in process.js will return the
    // same instance of the script and mixup previous downloads.
    scriptPromise = q.defer();
    promises = [];
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}
