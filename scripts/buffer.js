const turf = require('turf');
const lineChunk = require('turf-line-chunk');
const featureCollection = require('turf-featurecollection');
const buffer = require('turf-buffer');
const flatten = require('turf-flatten');
const ogr2ogr = require('ogr2ogr');
const fs = require('fs');
const q = require('q');
const readline = require('readline');

// custom scripts
const config = require('./config');
const tools = require('../lib/tools');
const split = require('./lineChunks');
const log = require('../lib/logger');
const docker = require('../lib/docker');

var levels = config.levels;
var distances = config.buffer.distances;
var outputFileName = config.buffer.nameTemplate; 
var scriptPromise = q.defer();
var bufferPromises = [];

function createBuffers(options, files, inputFormat) {
  files.forEach(function(file) {
    distances.forEach(function(distance) {
      var status = {
        id: file,
        promise: q.defer(),
        done: false 
      };
      bufferPromises.push(status);
      var selectStatement = "SELECT ST_Buffer(Geometry, "+distance+") from OGRGeoJSON";
      var geoFile = file;
    
      if(inputFormat === 'ESRI Shapefile') {
        var layerName = tools.getFileFromPath(file).split('.')[0];
        selectStatement = "SELECT ST_Buffer(Geometry ,"+distance+") from "+layerName;
      }
      log.debug(selectStatement);

      if(options.singleSided) {
        console.log('singleSided');
        coastline = JSON.parse(geoFile); 
        coastline = flatten(coastline);
        var cleaned = [];
        for(var i=0; i<coastline.features.length; i++) {
          var geometryType = coastline.features[i].geometry.type;
          if(geometryType != 'LineString') {
            //coastline.features.splice(i, 1);
            //i--;
            console.log(geometryType);
          } else {
            var coordinates = coastline.features[i].geometry.coordinates;
            // split polygon because single sided buffers cannot deal with closed ones
            if(coordinates[0][0] === coordinates[coordinates.length-1][0] && coordinates[0][1] === coordinates[coordinates.length-1][1]) {
              coastline.features[i].geometry.coordinates[0][0] -= 0.0000000001;
            }
            cleaned.push(coastline.features[i]);
          };
        }
        var outputDir = tools.getOutputDir(options);
        fs.writeFileSync(outputDir+'_test.geojson', JSON.stringify(featureCollection(cleaned)));
          selectStatement = 'SELECT ST_SingleSidedBuffer(Geometry,'+distance+', 1) from OGRGeoJSON';
      }
          
      var level = tools.getZoomFromFileName(file);
      var output = outputFileName;
      output = output.replace('{LEVEL}', level);
      output = output.replace('{DISTANCE}', distance);
      if(inputFormat === 'ESRI Shapefile') {
        output = output.replace('.geojson', '.zip');
      }
 
      var buffer = ogr2ogr(file)
        .format(inputFormat)
        .skipfailures()
        .options(['-dialect', 'sqlite', '-sql', selectStatement, '-nln', output.replace('.zip', '')])
        .timeout(10000000)
        .stream()
        buffer.pipe(fs.createWriteStream(options.outputDir+output));
        buffer.on('error', function(error) {
          console.log(error);
        });
        buffer.on('end', function() {
          status.done = true;
          console.log('buffer finished: '+output);
          if(tools.isAllDone(bufferPromises)) {
            // all done. ready to execute next step
            console.log('buffer ready');
            scriptPromise.resolve(bufferPromises);
          }
        });
      });
  });
}

var start = function(options, files, inputFormat) {
  if(options.levels) {
    levels = options.levels;
  }

  if(options.distances) {
    distances = options.distances;
  }
  
  createBuffers(options, files, inputFormat);
}

validateInputs = {
  inputFormats: ['geojson', 'shp', 'zip'],
  hasElements: [
    {docker: 'boolean'},
  ]
}

module.exports = {
  process: function(options) { 
    scriptPromise = q.defer();
    var inputFormat = '';

    // verify all inputs are available
    var files = tools.getFiles(options.inputs[0], '.geojson');
    log.warn(files);

    if(files.length === 0) {
      files = tools.getFiles(options.inputs[0], '.shp');
      if(files.length > 0) {
        inputFormat = 'ESRI Shapefile';
        log.info('Shapefiles found');
      } else {
        files = tools.getFiles(options.inputs[0], '.zip');
        if(files.length > 0) {
          inputFormat = "ZIP";
          log.info('ZIP found');     
        } else {
          log.info('No input files found (tried GeoJSON, Shape and zip)');
        }
      }
    } else {
      inputFormat = 'GeoJSON';
      log.info('GeoJSON found');
    }
 
    // verify configured levels match file namei
    var processFiles = [];
    options.levels.forEach(function(level) {
      for(var i=files.length-1; i>=0; i--) {
        if(files[i].indexOf(level) > -1) {
          processFiles.push(files[i]);
          files.splice(i, 1);
        }
      }
    });
    log.warn(processFiles);
    if(files.length > 0) {
      var error = 'No file for level '+level+' exist in '+options.inputs[0]+'. Please check outputs from previous steps.';
      log.error(error);
    }
    start(options, processFiles, inputFormat);

    return scriptPromise.promise;
  }
}
