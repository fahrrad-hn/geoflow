var turf = require('turf');
var lineChunk = require('turf-line-chunk');
var featureCollection = require('turf-featurecollection');
var buffer = require('turf-buffer');
var flatten = require('turf-flatten');
var ogr2ogr = require('ogr2ogr');
var fs = require('fs');
var q = require('q');
const readline = require('readline');

// custom scripts
var config = require('./config');
var tools = require('./tools');
var split = require('./lineChunks');

var levels = config.levels;
var distances = config.buffer.distances;
var outputFileName = config.buffer.nameTemplate; 
var scriptPromise = q.defer();
var promises = [];

function createBuffers(options, files, inputFormat) {
  if(options.clean) {
    files.forEach(function(file) {
      distances.forEach(function(distance) {
        var status = {
          id: file,
          promise: q.defer(),
          done: false 
        };
        var selectStatement = "'SELECT ST_Buffer(Geometry, "+distance+", 'endcap=flat join=round') from OGRGeoJSON'";
    
        if(inputFormat === 'ESRI Shapefile') {
          var layerName = tools.getFileFromPath(file).split('.')[0];
          selectStatement = "'SELECT ST_Buffer(Geometry ,"+distance+") from "+layerName+"'";
        }

        var command = "ogr2ogr -f 'GeoJSON' -dialect sqlite -sql "+selectStatement+" "+options.outputDir+tools.getFileFromPath(file)+"_"+distance+".geojson "+file
        console.log(command);
        var exec = require('child_process').exec;
        exec(command, function(error, stdout, stderr) {
          status.done = true;
          var allDone = tools.isAllDone(promises)
          if(allDone) {
            console.log('all buffers generated');
            scriptPromise.resolve(promises);
          }
        });
      });
    });
  } else {
    scriptPromise.resolve('done');
  }
}

var start = function(options, files, inputFormat) {
  if(options.levels) {
    levels = options.levels;
  }

  if(options.distances) {
    distances = options.distances;
  }
  
  createBuffers(options, files, inputFormat);
}

module.exports = function(options) {
  scriptPromise = q.defer();
  bufferPromises = [];
  var verify = {
    'numberOfInputs': 1
  };
  verifyOptions = tools.verifyOptions(options, verify);

  var inputFormat = '';
  // verify all inputs are available
  var files = tools.getFiles(options.inputs[0], '.geojson');
  if(files.length === 0) {
    files = tools.getFiles(options.inputs[0], '.shp');
    if(files.length > 0) {
      inputFormat = 'ESRI Shapefile';
      console.log('Shapefiles found');
    } else {
      console.log('No input files found (tried GoeJSON and Shape)');
    }
  } else {
    inputFormat = 'GeoJSON';
    console.log('GeoJSON found');
  }

  options.levels.forEach(function(level) {
    var found = false;
    // only files requested in options
    for(var i=files.length-1; i>=0; i--) {
      if(files[i].indexOf('_z'+level) > -1) {
        found = true;
      } else {
        files.splice(i, 1);
      }
    }

    console.log(files);
    if(!found) {
      var error = 'No file for level z'+level+' exist in '+options.inputs[0]+'. Please check outputs from previous steps.';
      verifyOptions = false;      
    }
  });

  if(verifyOptions) {
    start(options, files, inputFormat);
  } else {
    scriptPromise.reject('Options are not valid, please check them in chain.js');
  }

  return scriptPromise.promise;
}
