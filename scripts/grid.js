// Create buffer from generalized coastline data 
var turf = require('turf');
var lineChunk = require('turf-line-chunk');
var featureCollection = require('turf-featurecollection');
var buffer = require('turf-buffer');
var ogr2ogr = require('ogr2ogr');
var JSONStream = require('JSONStream');
var fs = require('fs');
var es = require('event-stream');

var tools = require('../lib/tools');

var q = require('q');
var scriptPromise = q.defer();
var promises = [];
var count = 0;

function start(options) {
  options.cellSize.forEach(function(cellSize) {
    var status = {
      id: options.cellSize,
      promise: q.defer(),
      done: false
    };
    promises.push(status);

    var grid = "";
    switch(options.type) {
      case 'hex':
        grid = turf.hexGrid(options.bbox, cellSize, options.units);
        break;
      case 'square':
        grid = turf.squareGrid(options.bbox, cellSize, options.units);
        break;
      case 'triangle':
        grid = turf.triangleGrid(options.bbox, cellSize, options.units);
        break;
      case 'point':
        grid = turf.pointGrid(options.bbox, cellSize, options.units);
        break;
    }

    var outputDir = tools.getOutputDir(options);
    var outputStream = fs.createWriteStream(outputDir+options.type+'_'+cellSize+'_'+options.units+'.geojson');

    es.readable(function (count, next) {
      for(var i=0; i<grid.features.length; i++) {
        this.emit('data', ['geometry', grid.features[i].geometry]);
      }
      this.emit('end');
    }).pipe(JSONStream.stringifyObject('{"type":"FeatureCollection","features":[{', '},{', '}]}')).pipe(outputStream);

    outputStream.on('finish', function () {
      status.done = true;
      console.log('grid generated: '+cellSize);
      if(tools.isAllDone(promises)) {
        console.log('grid ready');
        scriptPromise.resolve('grid ready');
      }
    }); 
  });
}

module.exports = {
  process: function(options) {
    scriptPromise = q.defer();
    
    if(options.clean) {
      start(options);
    } else {
       console.log('skipped, because of clean=false');
       scriptPromise.resolve({
         msg: 'hexbin skipped'
       });
    }

    return scriptPromise.promise;
  },
  benchmark: function(options) {
    scriptPromise = q.defer();
    promises = [];
    scriptPromise.resolve('benchmark executed');
    return scriptPromise.promise;
  }
}
