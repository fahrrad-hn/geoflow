var ogr2ogr = require('ogr2ogr');
var q = require('q');
var fs = require('fs');
var JSONStream = require('JSONStream');

// custom scripts
var tools = require('../lib/tools');
var scriptPromise = q.defer();
var promises = [];

var clipVector = function(options, target, source) {
 var status = {
   id: target,
   promise: q.defer(),
   done: false
  };
  promises.push(status);
  
  //var hexbinStream = fs.createReadStream(target);
  if(options.inputFormat === 'ZIP') {
    target = "/vsizip/"+target+"/"+tools.getFileFromPathWithoutExt(target).replace('_clipped', '')+".shp";
  }
  console.log(target);
  console.log(source);
/**
  var clip = ogr2ogr(target)
       .format('GeoJSON')
       .skipfailures()
       .options(['-clipsrc', source])
       .timeout(10000000)
       .stream()

   clip.pipe(fs.createWriteStream(options.outputDir+tools.getFileFromPath(source).replace('.geojson', '_clipped.zip')));
   clip.on('end', function() {
     status.done = true;
     if(tools.isAllDone(promises)) {
       // all done. ready to execute next step
       scriptPromise.resolve(promises);
      }
   });
*/
  var command = "ogr2ogr -f 'GeoJSON' -clipsrc "+source+" "+options.outputDir+tools.getFileFromPath(source).replace('.geojson', '_clipped.geojson')+" "+target;
  console.log(command);
 
  var exec = require('child_process').exec; 
  exec(command, function(error, stdout, stderr) {
    console.log(error);
    console.log(stdout);
    status.done = true;
    var allDone = tools.isAllDone(promises)
    if(allDone) {
      console.log('all buffers ready');
      scriptPromise.resolve(promises);
    }
  });
}

var start = function(options) {
  // hexbin
  var target = tools.getFiles(options.inputs[0], '.geojson');
  if(target.length === 1) {
    options.inputFormat = 'GeoJSON';
  } else {
    console.log('could not find geojson, try zip');
    target = tools.getFiles(options.inputs[0], '.zip');
    options.inputFormat = 'ZIP';
  }
  console.log(target);

  // buffer
  var source = tools.getFiles(options.inputs[1], '.geojson');
  if(source === 0) {
    source = tools.getFiles(options.inputs[1], '.zip');
  }
  console.log(source);
  console.log(target);

  source.forEach(function(polygon) {
    console.log('start');
    clipVector(options, target[0], polygon);
  });
}

module.exports = {
  process: function(options) {
    var verify = {
      'numberOfInputs': 2
    };
    verifyOptions = tools.verifyOptions(options, verify);
    console.log(options);

    if(verifyOptions) {
      if(options.clean) {
        start(options);
      } else {
        scriptPromise.resolve('skipped due to option clean=false');
      }
    } else {
      scriptPromise.reject('provided options are not valid. The script expects '+verify.numberOfInputs+' inputs.');
    }

    return scriptPromise.promise;
  }
}

