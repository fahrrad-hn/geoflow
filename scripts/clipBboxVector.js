'use strict'
const q = require('q')
const turf = require('turf');
const ogr2ogr = require('ogr2ogr');

// custom scripts
const log = require('../lib/logger');
const tools = require('../lib/tools');
const docker = require('../lib/docker');

var scriptPromise = q.defer();
var promises = [];

var clip = function(options, bbox) {
  var inputFiles = tools.getFiles(options.inputs[0], {filter: validateInputs.inputFormats[0], recursive: true});
  inputFiles.forEach(function(file) {
     var status = {
       id: file,
       promise: q.defer(),
       done: false
     };
     promises.push(status);

    var command = 'ogr2ogr -f "ESRI Shapefile" '+tools.getOutputDir(options)+tools.getFileFromPath(file)+' '+file+' -clipsrc '+bbox.join(" ");
    if(!options.docker) {
      tools.execCommand(command);
      scriptPromise.resolve('clipped');
    } else {
      docker.execGdal(command, '-d=false').then(function() {
        status.done = true;
        var allDone = tools.isAllDone(promises)
        if(allDone) {
          log.output('clip ready');
          scriptPromise.resolve(promises);
        }
      });
    }
  });  
}

var start = function(options) {
  var bbox = tools.convertBboxTo3857(options.bbox).then(function(result) {
    clip(options, result);
  });
}

var validateInputs = {
  numberOfInputs: 1,
  inputFormats: ['shp'],
  hasElements: [
    {bbox: 'object'},
    {docker: 'boolean'},
  ]
}

module.exports = {
  process: function(options) {
    start(options);
    return scriptPromise.promise;
  },
  validateInputs: validateInputs
}
