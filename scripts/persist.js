var q = require('q');
var fs = require('fs');
var tools = require('../lib/tools');

var scriptPromise = q.defer();
var promises = [];

var process = function(options) {
  var currentProcess = tools.getCurrentProcess();
  var output = tools.getOutputDir(options)+currentProcess.id;
  fs.writeFile(output+'.json', JSON.stringify(tools.getCurrentProcess(), null, 2), function(err) {
    if(err) {
        return console.log(err);
    }
  });

  fs.writeFile(output+'.md', tools.getMarkdown(), function(err) {
    if(err) {
      return console.log(err);
    }

    scriptPromise.resolve({
      msg: 'file saved'
    })
  });
}

module.exports = {
  description: 'save results in a final step',
  process: function(options) {
    scriptPromise = q.defer();
    process(options);
    return scriptPromise.promise;
  }
}
