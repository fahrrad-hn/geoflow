FROM node:latest

USER root
RUN apt-get -qq update \
   && apt-get -qq -y install \
   curl sqlite3 libsqlite3-dev

RUN curl -sSL https://get.docker.com/ | sh

#RUN usermod -a -G docker "$USER"

#CMD ["docker", "--version", "node", "--version"]
CMD docker --version ; echo "node version: " ; node --version; echo "npm version: " ; npm --version;
