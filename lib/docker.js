'use strict'
const q = require('q');
const cp = require('child_process');

// custom scripts
const log = require('./logger');
const tools = require('./tools');

var gdalImage = 'geodata/gdal';
var tippecanoeImage = 'jskeates/tippecanoe';
var tileserverImage = 'klokantech/tileserver-gl';

var execGdal = function(cmd, options) {
  return exec(gdalImage, cmd, options);
}

var execTippecanoe = function(cmd, options) {
  return exec(tippecanoeImage, cmd, options);
}

var execTileserver = function(cmd, options) {
  return exec(tileserverImage);
}

var exec = function(imageName, cmd, options) {
  log.info('Execute with docker')
  var deferred = q.defer();
  if(!imageName || !cmd) {
    var msg = 'please specify imageName and cmd'
    log.warn(msg);
    deferred.reject( {msg: msg });
  } else {
    if(options === undefined) {
      options = "-d";
    } else {
      if(options.indexOf('-d') === -1) {
        options += " --rm "
      }
    }
    // echo exit after command finished, ready to resolve promise
    var dockerCmd = "docker run "+options+" -i -l 'processing' -v $(pwd):/data -w /data "+imageName+" "+cmd;
    log.output('\n'+dockerCmd); 
    var container = cp.exec(dockerCmd, function(err, stdout, stderr) {
      if(err) {
        log.error('Docker error: '+err);
	deferred.reject({msg: err});
      }
      //log.warn(stdout);
      //deferred.resolve(container);
    });
    container.stdout.on('data', function(data) {
      log.output(tools.trim(data.toString()));
      /*if(tools.trim(data.toString()) === 'exit') {
        log.debug('exit detected');
	deferred.resolve(container);
      };*/
    });
    container.setMaxListeners(0);
    container.on('exit', function(data) {
      // hacky timout, needs to wait until files are written. How to check?
      setTimeout(function() {
        deferred.resolve(container);
      }, 1000);
    });
  }
  return deferred.promise;
}

var attach = function(containerId) {
  var command = "docker attach "+containerId;
  log.info('attach command: '+command);
  var container = cp.exec(command, function(err, stdout, stderr) {
    if(err) {
      log.error(err);
    }
    log.warn('attached to container: '+containerId);
    log.warn(stdout);
  });
  container.stdout.on('data', function(data) {
    log.warn(data.toString());
  });
}

process.setMaxListeners(20);
process.on('SIGINT', function() {
  var label = "processing";
  var containers = list(label);
  log.line();
  log.warn('Exit.');
  log.info('Clean docker containers...');
  containers.forEach(function(container) {
    stop(container);
    remove(container);
  });
  setTimeout(function() {
    containers = list(label); 
    containers.forEach(function(container) {
      log.warn('Remaining container: '+container)
    }); 
    log.warn('Exit done.');
    process.exit();
  }, 2000);
});

var list = function(label) {
  var cp = require('child_process');
  var containerList = cp.execSync("docker ps -a -q --filter 'label="+label+"'").toString('UTF-8');
  var containerArray = containerList.split("\n");
  containerArray.pop();
  return containerArray;
}

var stop = function(containerId) {
  if(containerId) {
    var command = "docker stop -t 0 "+containerId;
    log.debug('Stop container with id: '+containerId);
    log.debug(command);

    cp.exec(command, function(err, stdout, stderr) {
      if(err) {
        log.error('Failed to stop container with id '+containerId+': '+err);
      } else {
        log.debug('Container stopped: '+containerId);
      }
    });
  } else {
    log.debug('no container started, nothing to stop');
  }
}

var remove = function(containerId) {
  if(containerId) {
    var command = "docker rm "+containerId;
    cp.exec(command, function(err, stdout, stderr) {
      if(err) {
        log.error('Failed to remove container with id '+containerId+': '+err);
      } else {
        log.debug('Container removed: '+containerId);
      }
    });
  } else {
    log.debug('no container id provided');
  }
}
 
module.exports = {
  execTippecanoe: execTippecanoe,
  execGdal: execGdal,
  attach: attach,
  stop: stop,
  list: list
}
