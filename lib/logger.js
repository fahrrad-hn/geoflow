'strict'

var logLevel = '';
const RED='\033[0;31m'
const GREEN='\033[0;32m'
const CYAN='\033[0;36m'
const LIGHTRED='\033[1;31m'
const YELLOW='\033[1;33m'
const NC='\033[0m' // No Color

var setLogLevel = function(level) {
  // ensure logLevel is one of error/warning/info/debug
  debug('set log level to: '+level);
  logLevel = level;
}

var error = function(text) {
  if(logLevel === 'debug' || logLevel === 'info' || logLevel === 'warning' || logLevel === 'error') {
    console.log(RED+'ERROR>\t'+NC+text);
  }
}

var warn = function(text) {
  if(logLevel === 'debug' || logLevel === 'info' || logLevel === 'warning' || logLevel === 'error') {
    console.log(LIGHTRED+'WARN>\t'+NC+text);
  }
}

var debug = function(text) {
  if(logLevel === 'debug' || logLevel === 'warning' || logLevel === 'error') {
    console.log(YELLOW+'DEBUG>\t'+NC+text);
  }
}

var info = function(text) {
  if(logLevel === 'info' || logLevel === 'warning' || logLevel === 'error') {
    console.log(GREEN+'INFO>\t'+NC+text);
  }
}

var output = function(text) {
  console.log(CYAN+'OUTPUT>\t'+text+NC);
}

var log = function(text) {
  console.log('LOG>\t'+text);
}

var line = function() {
  console.log("");
}

module.exports = {
  setLogLevel: setLogLevel,
  debug: debug,
  info: info,
  error: error,
  warn: warn,
  output: output,
  log: log,
  line: line
}
