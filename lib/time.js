const log = require('./logger');

var _times = {};

var time = function(label) {
  _times[label] =  Date.now();
};


var timeEnd = function(label) {
  var time = _times[label];
  if (!time) {
    log.warn('Time not started for: '+label);
    throw new Error('No such label: ' + label);
  }
  var duration = (Date.now() - time) / 1000;
  log.info(label+': '+duration+'s');
  return duration;
};

module.exports = {
  time: time,
  timeEnd: timeEnd
}
