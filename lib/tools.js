const zip = require("yauzl");
const fs = require('fs-extra');
const path = require('path');
const turf = require('turf');
const ogr2ogr  = require('ogr2ogr');
//const ogr2ogr = '';// = require('ogr2ogr');
const json2md = require('json2md');
const log = require('./logger');
const q = require('q');
const proj4 = require('proj4');

var processing = {};
var chain = [];

var initProcess = function(chain) {
  //log.debug('init: '+JSON.stringify(chain, null, 2));
  chain = chain;

  var logLevel = chain.config.logLevel;
  if(logLevel) {
    log.setLogLevel(logLevel);
    log.info('LogLevel: '+logLevel);
  }

  for(var i=0; i<chain.steps.length; i++) {
    chain.steps[i].process = {
      state: 'todo',
      executionTimeSeconds: -1,
      startTime: -1,
      endTime: -1
    }
  };

  processing = {
    id: generateId(),
    description: chain.description,
    date: new Date().toISOString(),
    steps: chain.steps,
    result: {}
  }
}

function setStepState(stepId, process) {
  var step = getStepById(stepId);
  if(step && (process.state === 'todo' | process.state === 'processing' | process.state === 'finished' | process.state === 'failed')) {
    step.process = process;
  } else {
    log.error('could not find '+stepId+' or state is not one of todo, finished or failed, state='+state);
  }
}

function getStatus() {
  var steps = processing.steps;
  var hit = "";
  steps.forEach(function(step) {
    if(step.process.state === "processing") {
      hit = step.id;
    }
  });
  return hit;
}

function getMarkdown() {
  var markdown = [];
  var steps = processing.steps;
  markdown.push({h1: 'Overview'});
  markdown.push({p: 'ID: '+processing.id+', Date: '+processing.date});
  markdown.push({code: {
    language: 'mermaid',
    content: getGraph().split('\n')
  }});
  if(processing.description) {
    markdown.push({p: processing.description});
  };
  for(var i=0; i<steps.length; i++) {
    var step = steps[i];
    markdown.push({h1: step.id});
    markdown.push({p: step.description});
    //markdown.push({h2: 'Details'});
    var table = [];
    for (var key in step) {
      if (step.hasOwnProperty(key)) {
        var value = step[key];
        if(typeof value === 'object') {
          value = JSON.stringify(value);
        }
        table.push([key, value.toString()]);
      }
    }
    markdown.push({table: { headers: ["key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "value"], rows: table } });
  }
  return json2md(markdown);
}

function getGraph() {
  var graph = 'graph LR\n'
  var steps = processing.steps;
  for(var i=0; i<steps.length; i++) {
    if(steps[i+1]) {
      graph += ''+steps[i].id+'-.->|Step '+steps[i].number+'|'+steps[i+1].id+'\n';
    }
    log.output(JSON.stringify(steps[i], null, 2));
    if(steps[i].inputs) {
      var inputs = steps[i].inputs;
      for(var j=0; j<inputs.length; j++) {
        if(typeof inputs[j] === 'object') {
          graph += inputs[j].fileName+'---|INPUT|'+steps[i].id+'\n';
        } else {
          graph += getScriptIdFromDir(inputs[j])+'---|INPUT|'+steps[i].id+'\n';
        }
      }
    }
  }
  log.info('Graph generated');
  return graph;
}

function getScriptIdFromDir(dir) {
  var scriptId;
  if(dir) {
    scriptId = dir.split('_');
    if(scriptId[1]) {
      scriptId = scriptId[1].replace('/', '');
    }
  } else {
    log.warn('Could not extract scriptId from passed directory name: '+dir);
  }
  return scriptId;
}

function generateId() {
  var text = "";
  var possible = "abcdefghijklmnopqrstuvwxyz";

  for( var i=0; i < 5; i++ ) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

function getStepById(stepId) {
  var step;
  for(var i=0; i<processing.steps.length; i++) {
    if(processing.steps[i].id === stepId) {
      step = processing.steps[i];
    }
  }
  return step;
}

var setStepResult = function(stepId, result) {
  var step = getStepById(stepId);
  if(step) {
    step.result = result;
  }
}

var getCurrentProcess = function() {
  return processing;
}

var getOutputs = function(scriptId) {
  var dir = getOutputDir(scriptId);
  var result = [];
  files = fs.readdirSync(dir);
  files.forEach(function(fileName) {
    var sizeInMb = fs.statSync(dir+fileName).size / 1000000.0;
    var fileItem = {};
    fileItem[fileName] = sizeInMb;
    result.push(fileItem);
  });
  return result;
}

var getFiles = function(dir, options) {
  var filter = '';
  var recursive = '';

  if(options) {
    filter = options.filter === undefined ? '': options.filter;
    recursive = options.recursive === undefined ? '': '-maxdepth 5';
  }

  var result = [];
  if (!fileExists(dir)) {
    log.warn('Directory ' + dir + ' does not exist');
    return
  }

  dir = getDirWithSlash(dir);

  var execSync = require('child_process').execSync;
  var command = 'find '+ dir + ' '+recursive+' -type f '+'-name "*'+ filter +'"';
  log.debug(command);

  var commandResult = execSync(command).toString();
  var result = commandResult.split('\n');
  result = result.filter(function(n){ return n != '' });
  log.debug(result);

  return result;
}

var fileExists = function(file) {
  return fs.existsSync(file);
}

var unzip = function(input, output) {
  return fs.createReadStream(input).pipe(zip.Extract({ path: output }));
}

var prepareDir = function(options) {
  log.debug('Prepare directory:\t'+JSON.stringify(options, null, 2));
  createTmp();

  if(options.clean === undefined || options.clean) {
    cleanDir(getOutputDir(options));
  } else {
    log.debug('Do not clean dir for:\t'+options.script);
  }

  createDir(getOutputDir(options));
}

var getOutputDir = function(options) {
  var stepNumber = options.number;
  if(stepNumber < 10) {
    stepNumber = 0+''+stepNumber;
  }
  var dir = getDirWithSlash('./data/'+stepNumber+'_'+options.script);

  if(options.id) {
    dir = getDirWithSlash('./data/'+stepNumber+'_'+options.id);
  }

  return dir;
}

var copyFile = function(src, dst) {
  log.warn('copy file from '+src+' to '+dst);
  fs.writeFileSync(dst, fs.readFileSync(src));
  return;
}

var copyDir = function(src, dst) {
  log.warn('copy dir from '+src+' to '+dst);
  fs.copy(src, dst);
  return;
}

var getInputs = function(options) {
  log.debug('getInputs '+JSON.stringify(options, null, 2));
  var inputs = []

  if(options.inputs) {
    for(var j=0; j< options.inputs.length; j++) {
      for(var i=0; i < steps.length; i++) {
        if(steps[i].number != undefined) {
          // detect type of input, could be a string referencing
          // input folder or a object with more opitons e.g. see
          // options of download step
          if(typeof options.inputs[j] === 'object') {
            //inputs.push(options.inputs[j]);
          } else {
            if(options.inputs[j].indexOf(steps[i].id) > -1) {
              var outputDir = getOutputDir(steps[i]);
              inputs.push(outputDir);
            }
          }
        }
      }
    }
    log.debug('Detected inputs:\t'+inputs);
    return inputs;
  } else {
    log.debug('no inputs defined, use outputs from previous steps as input');

    if(steps[options.number-1]) {
      // if no inputs are defined return the outputs from the previous step
      return [getOutputDir(steps[options.number-1])];
    } else {
      // if there is no previous step return empty
      log.debug('this seems to be the first step, nothing to use as an input. You could link a static input');
      return []
    }
  }
}

var getFileFromPath = function(filePath) {
  return path.basename(filePath);
}

var getPathFromFile = function(filePath) {
  return path.dirname(filePath);
};

var getFileFromPathWithoutExt = function(filePath) {
  var file = getFileFromPath(filePath);
  return file.replace(/\.[^/.]+$/, "");
}

var deleteFiles = function(files) {
  files.forEach(function(file) {
    fs.unlink(file);
  });
}

var createTmp = function() {
  createDir('./data');
}

var cleanTmp = function() {
  log.debug('Delete directory:\t'+dir);
  cleanDir('./data');
}

var createDir = function(dir) {
  log.debug('Create directory:\t'+dir);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
}

var cleanDir = function(dir) {
  // r for recursive, f ignore the dir does not exists
  log.debug('Clean directory:\t'+dir);
  execCommand('rm -rf '+dir);
}

var execCommand = function(command) {
  var execSync = require('child_process').execSync;
  log.debug('Execute command:\t'+command);
  return execSync(command).toString();
}

var getDirWithSlash = function(dir) {
  if(!dir.endsWith('/')) {
    dir += '/';
  };

  return dir;
}

var isAllDone = function(promises) {
  var status = [];

  promises.forEach(function(promise) {
    status.push(promise.done);
  });

  return (status.indexOf(false) > -1 ? false:true);
}

var validateOptions = function(step, script) {
  var result = true;

  if(script.validateInputs) {
    log.info('Validate options. Script exports validateInputs: '+JSON.stringify(script.validateInputs));
    if(step.inputs) {
      if(step.inputs.length != script.validateInputs.numberOfInputs) {
        log.debug('no inputs defined, try to use inputs from previous step');
      }
    }
    if(script.validateInputs.inputFormats) {
      log.debug('Verify that at least one input file is available with file extension: '+script.validateInputs.inputFormats[0]);

      if(step.inputs.length === 0) {
	log.error('No input defined');
	result = false;
      }
      step.inputs.forEach(function(dir) {
        var files = getFiles(dir, {filter: script.validateInputs.inputFormats[0]});
	if(files.length === 0) {
          log.error('No input files found in '+dir+' with extension '+script.validateInputs.inputFormats);
          result = false
        } else {
          log.debug(files.length+' input files found with extension '+script.validateInputs.inputFormats[0]+': '+files);
        }
      });
    }
    // validate mandatory options, check for availability and type
    if(script.validateInputs.hasElements) {

      script.validateInputs.hasElements.forEach(function(element) {
        for (var key in element) {
          if (element.hasOwnProperty(key)) {
            if(step[key] === undefined) {
	      log.error('Did not find key '+key+' in options');
	      result = false;
	    } else {
	      log.debug('key: '+key+', value: '+step[key]);
	      if(typeof step[key] !== element[key]) {
	        log.error('Expected '+key+ ' to be type '+element[key]+', but was '+(typeof step[key]));
	        result = false;
	      }
	    }
          }
	}
      });
    }
  } else {
    log.info('Config Validation IS DEACTIVATED. The script does not export validateInputs.');
  }

  return result;
}

var getExecutionTime = function(begin, end) {
  return (end-begin) / 1000;
}

var getZoomFromFileName = function(fileName) {
  var zoom = '';
  var fileNameSplit = fileName.split('_');
  fileNameSplit.forEach(function(part) {
    if(part.indexOf('z') === 0) {
      zoom = part[1];
    };
  });

  return zoom;
}

var convertBboxToWgs84 = function(bbox) {
  var firstProjection = "+proj=moll +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs";
  var secondProjection = "EPSG:4326";
  return convertBbox(firstProjection, secondProjection, bbox);
}

var convertBboxTo3857 = function(bbox) {
  var firstProjection = "EPSG:4326";
  var secondProjection = "EPSG:3857";
  return convertBbox(firstProjection, secondProjection, bbox);
}

var convertBboxToMollweide = function(s_srs, bbox) {
  var firstProjection = s_srs;
  var secondProjection = "+proj=moll +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m no_defs"
  return convertBbox(firstProjection, secondProjection, bbox);
}

var convertBbox = function(firstProjection, secondProjection, bbox) {
  var promise = q.defer();

  var lowerLeft = proj4(firstProjection,secondProjection,[bbox[0], bbox[1]]);
  var upperRight = proj4(firstProjection,secondProjection,[bbox[2], bbox[3]]);

  log.debug([lowerLeft[0], lowerLeft[1], upperRight[0], upperRight[0]]);
  promise.resolve([lowerLeft[0], lowerLeft[1], upperRight[0], upperRight[1]]);

  return promise.promise;
}

var validateConfig = function(config) {
  var result = [];
  for(var i=0; i<config.length; i++) {
    var stepConfig = config[i];
    stepConfig.validation = [];

    // every config needs to have an id...
    if(!stepConfig.id) {
      stepConfig.validation.push('id is missing');
    };
    // ... and a script tag
    if(!stepConfig.script) {
      stepConfig.validation.push('script is missing');
    };
    if(stepConfig.validation.length > 0) {
      result.push(stepConfig);
    }
  }
  return result;
}

var trim = function(text) {
  return text.trim().replace(/(\r\n|\n|\r)/gm,"");
}

var renameFiles = function(dir, renameOptions) {
  renameOptions.forEach(function(option) {
    log.warn('rename files in folder '+dir+' from '+option.from+' to '+option.to);
    var cmd = "rename 's/"+option.from+"\\./"+option.to+"\\./' "+dir+"/*";
    log.warn(cmd);
    exec(cmd);
  });
}

var exec = function(cmd) {
  var deferred = q.defer();
  log.info('execute cmd: '+cmd);
  var cp = require('child_process');
  cp.exec(cmd, function(error, stdout, stderr) {
    if(error) {
      log.error(error);
      deferred.reject();
    } else {
      deferred.resolve(stdout);
    }
  })
  return deferred.promise;
}

module.exports = {
  initProcess: initProcess,
  getCurrentProcess: getCurrentProcess,
  setStepResult: setStepResult,
  setStepState: setStepState,
  getMarkdown: getMarkdown,
  getStatus: getStatus,
  getGraph: getGraph,
  getOutputs: getOutputs,
  getFiles: getFiles,
  deleteFiles: deleteFiles,
  prepareDir: prepareDir,
  getInputs: getInputs,
  getOutputDir: getOutputDir,
  getFileFromPath: getFileFromPath,
  getFileFromPathWithoutExt: getFileFromPathWithoutExt,
  isAllDone: isAllDone,
  cleanTmp: cleanTmp,
  unzip: unzip,
  execCommand: execCommand,
  fileExists: fileExists,
  validateOptions: validateOptions,
  getExecutionTime: getExecutionTime,
  getZoomFromFileName: getZoomFromFileName,
  convertBboxToWgs84: convertBboxToWgs84,
  convertBboxToMollweide: convertBboxToMollweide,
  convertBboxTo3857: convertBboxTo3857,
  validateConfig: validateConfig,
  createDir: createDir,
  getPathFromFile: getPathFromFile,
  copyFile: copyFile,
  copyDir: copyDir,
  trim: trim,
  exec: exec,
  renameFiles: renameFiles,
}
