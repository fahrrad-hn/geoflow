self.onmessage = function(event) {
  var classOfInterest = event.data.classOfInterest;
  var lineNumber = event.data.lineNumber;
  var data0 = event.data.data0;
  var data1 = event.data.data1;
  var detected = [];

  for(var i=0; i < data0.length; i++) {
    if(data0[i] != data1[i] && (data0[i] == classOfInterest || data1[i] == classOfInterest)) {
      detected.push(i);
    }
  }
  postMessage({'lineNumber': lineNumber, 'position': detected}, [Uint8Array.from(detected).buffer]);
  var detected = [];
  self.close();
};
