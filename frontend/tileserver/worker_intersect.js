var window = self;

importScripts('lib/turf.min.js');
importScripts('lib/jsts-1.3.min.js');

self.onmessage = function(e) {
  var buffer = JSON.parse(e.data.buffer);
  var mangrove = turf.featureCollection(JSON.parse(e.data.mangrove));

  var multiPoly = turf.multiPolygon([]);
  // filter invalid topology (self-intersection) for polygons
  for(var i=0; i<mangrove.features.length; i++) {
    if(mangrove.features[i].geometry.type=='Polygon') {
      var isIntersect = detectIntersect(mangrove.features[i]);
      if(!isIntersect) {
         multiPoly.geometry.coordinates.push(mangrove.features[i].geometry.coordinates);
      }
    } else if(mangrove.features[i].geometry.type=='MultiPolygon') {
      for(var j=0; j<mangrove.features[i].geometry.coordinates.length; j++) {
        for(var k=0;k<mangrove.features[i].geometry.coordinates[j].length;k++) {
          /*
              Possible topologic problems:
                - self intersecting polygons
                - Multipolygon with overlapping polygon
          */
          var isIntersect = detectIntersect(mangrove.features[i]);
          if(!isIntersect) {
            multiPoly.geometry.coordinates.push([mangrove.features[i].geometry.coordinates[j][k]]);
          }
        }
      }
    } else {
      console.info('detected other: '+mangrove.features[i].geometry.type)
    }
  }

  function detectIntersect(polygonFeature) {
    var isIntersect = false;
    var kinks = turf.kinks(polygonFeature);
    if(kinks.features.length > 0) {
        isIntersect = true;
    }

    return isIntersect;
  }
  
  /**
  var featureCollection = turf.featureCollection([]);
  var mangroveInsideBuffer = turf.featureCollection([]);
  var turfErrors = turf.featureCollection([]);
  console.info(turfErrors);
  for(var i=0; i<multiPoly.geometry.coordinates.length; i++) {
    var polygon = turf.polygon(multiPoly.geometry.coordinates[i]);
    var intersect = false;
    try {
      intersect = turf.intersect(polygon, buffer);
    } catch(e) {
      console.info(e);
      var point = turf.point([e.pt.x, e.pt.y], {message: e.message, name: e.name});
      turfErrors.features.push(point);
    }
    if(intersect) {
      mangroveInsideBuffer.features.push(intersect);
      postMessage({'mangroveInsideBuffer': mangroveInsideBuffer});
    }
  }
  console.log(JSON.stringify(turfErrors));
  */

  var mangroveInsideBuffer = turf.intersect(turf.buffer(multiPoly, 0), buffer);
  postMessage({'mangroveInsideBuffer': mangroveInsideBuffer});
};