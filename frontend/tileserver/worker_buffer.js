var window = self;

importScripts('lib/turf.min.js');
importScripts('lib/jsts-1.3.min.js');

self.onmessage = function(e) {
  var coastline = turf.featureCollection(JSON.parse(e.data.coastline));
  var coastlineBuffer = turf.buffer(coastline, e.data.bufferSize, 'kilometers');
  
 
  // hormonize: coastline data is sometimes a feature, sometimes a featurecollection.
  // if it is a feature it is converted to a featureCollection
  var polygonUnion = [];
  if(!coastlineBuffer.features) {
    console.info('Convert coastline feature to featureCollection');
    coastlineBuffer = turf.featureCollection([coastlineBuffer]);
    polygonUnion.push(coastlineBuffer.features[0])
  }
  polygonUnion.push(coastlineBuffer.features[0]);

  for(var i=1; i<coastlineBuffer.features.length; i++) {
    polygonUnion[0] = turf.union(polygonUnion[0], coastlineBuffer.features[i]);
  }
  polygonUnion = polygonUnion[0]; 

  postMessage({'coastlineBuffer': polygonUnion});
};