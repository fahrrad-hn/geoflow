function stopServer() {
  var callback = function(response) {
    console.log(response);
  }
  request('/server/close', callback);
}

function getStatus() {
  var callback = function(response) {
    var elem = document.getElementById(response);
    var pos = elem.getAttribute('transform').replace("translate(", "").replace(")", "");
    pos = pos.split(',');
    console.log(pos);
    //panZoom.zoomAtPoint(2, {x: pos[1], y: pos[0]});
    var elem = document.getElementById(response);
    var position = $(elem).offset();
    var parentOffset = $(document.getElementById('panel')).offset();

    position.top -= parentOffset.top;
    position.left -= parentOffset.left;
    
    console.log(position.top);
    console.log(position.left);

    {
      let width = $(container).width();
      let height = $(container).height();

      this.panZoom.panBy({x: width/2 - position.left, y: height/2 - position.top});
    }
  }
  request('/status', callback);
}

(function poll(){
  setTimeout(function() {
    getStatus();
    poll();
  }, 5000);
})();

function requestGraph() {
  /**var graphCallback = function(response) {
    var mermaidholder = document.getElementById('mermaid');
    console.log(mermaidholder);
    var mermaidnode = document.createElement('div');
    mermaidnode.className = 'mermaid';
    mermaidnode.appendChild(document.createTextNode(response));
    mermaidholder.appendChild(mermaidnode);
    mermaid.init(); // jshint ignore:line
    //console.log(response);
  }
  request('/graph', graphCallback);
  */
  
  var markdownCallback = function(response) {
    var md = window.markdownit({html:true, linkify: true, typographer: true,});
    var markdownHtml = md.render(response)
    var container = document.getElementById('container');
    container.innerHTML = markdownHtml;
    renderGraphs();
  }
  request('/process.md', markdownCallback); 
}

var saveGraph = function(id) {
  var svg = document.getElementById(id);
  var download = getDownloadURL(svg);
  console.log(download);
}

function getDownloadURL(svg) {
  var canvas = document.createElement('canvas');
  canvas.width = 100;
  canvas.height = 100;
  var serializer = new XMLSerializer();
  var svgString = serializer.serializeToString(svg);
  canvg(canvas, svgString);
  console.log('here');
  return canvas.toDataURL('image/png');
}

var getExportButton = function() {
  var exportButton = document.createElement("BUTTON");
  exportButton.id = 'panel';
  var text = document.createTextNode("Save");
  exportButton.appendChild(text);
  exportButton.onclick = function(event){
    saveGraph(event.target.id);
  };
  exportButton.setAttribute('style', 'width:80px;height:30px');
  return exportButton;
}

var panZoom = "";
function renderGraphs() {
  var graphs = document.getElementsByClassName('language-mermaid');
  for(var i=0; i<graphs.length; i++) {
    console.log(graphs[i]);
    //graphs[i].parentNode.appendChild(getExportButton());
    mermaidAPI.render('panel', graphs[i].innerHTML.replace(/&gt;/g,'>'), function(svg) {
     graphs[i].innerHTML = svg;
     graphs[i].firstChild.setAttribute('style', 'width:100%');
     graphs[i].firstChild.removeAttribute('height');
    });
  };
  panZoom = svgPanZoom('#panel', {
    zoomScaleSensitivity: 1,
    minZoom: 0.5,
    maxZoom: 2.5,
    fit: true,
    contain: true,
    center: true,
    refreshRate: 'auto',
  });
}

function request(url, callback) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      callback(xmlHttp.responseText);
  }
  xmlHttp.open("GET", url, true); // true for asynchronous
  xmlHttp.send(null);
}
