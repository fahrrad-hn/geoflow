# Introduction
This is a process exectution framework written in nodejs to do geoprocessing in a modular, reusable manner.
The goal is to support processing via single scripts, chained together with this framework to achieve the processing target. The scripts can be reused by different configurations.

## Wording
- step: a single script which has inputs and will put its outputs to a folder
- chain: concattination of different steps
- workflow:

## Dependencies
The main dependency is nodejs and different nodejs modules (see package.json). Each step can has own dependencies which can be native programs.

It is possible to run the processing framework with docker. It will install all dependencies by itself.

# Usage
- adapt settings in chain.js
- start processing:
```
node --max_old_space_size=4000 process.js chain
```
The last argument (chain) references the file which defines the processing chain configuration. The framework will search for a chain.js config file in the config folder. It is also possible to specify config/chain.js directly.

## Architecture
### Principles
- Will execute step after step (e.g. Step2 is only executed when Step1 finished completly).
- The files in the output folder are the inputs for the following steps.


## Docker
Running the framework within docker has several advantages:
* execute everywhere where docker is available, no further dependencies
* controlled environment
* easy to exchange dependency versions

```
sh ./run_with_docker.js
```
This script will spin up a docker container with npm installed and mount the complete framework inside the container (mountpoint is /data inside the container). It also has docker installed, so it is possible to run docker containers.

One important part to note is the following part.
```
-v /var/run/docker.sock:/var/run/docker.sock
```

Now this container will have access to the Docker socket, and will therefore be able to start containers. Except that instead of starting “child” containers, it will start “sibling” containers.

## Configuration file
```Javascript
var download = {
  'script': 'download',
  'id': 'downloadPopulation',
  'description': 'Download population data',
  'clean': true,
  'urls': [
  {
    'path': 'http://example.com/',
    'fileName': 'geodata.zip'
  }]
}

var clip = {
  'script': 'clipBboxRaster',
  'id': 'clipBboxRaster',
  'inputs': ['downloadPopulation'],
  'bbox': [ 60, 5.8, 140, 43 ],
  'clean': true,
  'description': 'Clips raster data with a bounding box'
}


var chain = [download, clip]
```
The code snippet above is an example configuration for downloading and clipping raster data. The most important part is the "chain"-Variable at the very end. It defines the order of script execution. In the example it will first download the data and then clip it.

### General config
```Javascript
var config = {
  scriptPath: './scripts/',
  outputPath: './data/',
  logLevel: 'info'
}
```

* scriptPath: folder to search for the scripts to execute
* outputPath: folder to put the outputs. This can be useful if you run two or more instances at the same time.
* logLevel: set the log level. Possible values are 'info', 'debug'

### Preprocessed Inputs
Some steps can have inputs which rarely change and can be time consuming to generate. If this is the case just drop them in the ./input folder and reference them as following in the script

```
var staticInput = {
  ...
  'inputs': ['../static/00_staticInput'],
  'clean': false,
  ...
}
```
The important part here is the "clean: false" option. It will prevent the deletion of the folder content and skip this step.

### Script Options
In the options there are parts which are supported by all script and there are parts which are needed only for the specific scripts.

#### Valid options for all scripts
- **id**: needed to reference inputs from other steps. For example the *clip* step defines the id *downloadPopultion* as an input.
- **script**: refers to the file name of the script. For example the step with the id *downloadPopulation* executes a script the file *download.js* as defined in the script tag.
- **description**: the is the description of the specific step. While the script *download* has e.g. the description *download data and extract zip* a description of a step could be *download population data from GHS*
- **inputs**: define the inputs for the script. The input is referenced by the id of other scripts. For some scripts multiple inputs can be defined. If there are no inputs defined the framework will try to use the data from the previous step. It is recommendated to define inputs explicitly.
- **clean**: If set to 'true' it will delete the folder from previous execution. If set to 'false' will not delete the folder so the outcome from previous execution can be reused. Mainly this helps for the development of a new chain.
- **valideInputs**: *OPTIONAL*, this configuration enables input validation and ensures if e.g. the number of inputs is correct before the script is executed. Supported values (all optional):
*numberOfInputs*: verifies the number of inputs specified in the config file
*bbox*:
*inputFormats*: list of allowed input formats. It verifies that at least one file is available in the requested format.
*hasElements*: will check the config if the key defined here is available and of specified type: e.g. 'hasElements': [{docker: 'boolean'}] will verify that there is a docker key of type boolean.

# Features
- Support nice documention of your scripts: for every step there is a description tag in the configuration which describes the goal of the configuration
- Dockerized
- Verify inputs and outputs
- Statistics export: The framework gives you nice statistics for the processing without the need to implement the functions by yourself. For example it calculates the time neeed to execute a step.
- Benchmark mode: The benchmark mode is mainly for development of new scripts. If the script exports a benchmark function you can optimize your processing steps during development to speed. The special is that the benchmark will generate all inputs with predefined inputs before executing the actual benchmark.
The benchmark can be configured via benchmark.js

# Development
## Script description
```Javascript
module.exports = {
  ...
  description: 'This is the description of what the script does'
  ...
}
```

## Implement processing
```Javascript
module.exports = {
  ...
  process: function(options) {
    // here comes your process
    // return a promise
  }
}
```

## Implement benchmark
Inside your script export a benchmark function.

```Javascript
module.exports = {
  ...
  benchmark: function(options) {
    // here comes your benchmark
    // return a promise
  }
  ...
}
```

# Overview
col1 | col2
# server

# Handling large files
Handling large files can be tricky. For example global data in GeoJSON format can easily exceed 1GB of storage. Parsing this file will cause the parset probably to eat up available memory. Different stragegies to handle these files will be discussed here.

## Streaming
One possible solution is to use streaming for reading and writing files

### inputs
```Javascript

var streamOutput = ogrStream.pipe(JSONStream.parse('features.*'));
```

# Planned Features
- Notifications for exectution start/end via mail/xmpp. notification start gives a http link to access the current status
- Generate nice output as markdown. This includes the statistics (execution time, file size) and a graphical overveiw over the steps
