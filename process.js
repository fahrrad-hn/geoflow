const fs = require('fs');
var tools;
var time;
var log;
const exec = require('child_process').exec

var scriptPath = './scripts/'
var steps = "";
var i = 0;

var requireScript = function(step) {
  step.number = i;
  var name = step.script;

  var processing = {
    state: 'processing',
    executionTimeSeconds: -1,
    startTime: Date.now(),
    endTime: -1
  }
  tools.setStepState(step.id, processing);

  log.info('');
  log.info('---');
  log.info(step.number + '. ' + step.script + ' - '  + step.description);
  log.info('---');
  log.info('');

  time.time(name+' execution time')
  tools.prepareDir(step);
  // this replaces the input script id with the actual folder of inputs
  step.inputs = tools.getInputs(step);
  var script = require(scriptPath + step.script);

  log.debug('Execute step:\t\t'+step.id);
  log.debug('Details: '+JSON.stringify(step, null, 2));

  if(step.clean) {
    validateOptions = tools.validateOptions(step, script);
    if(validateOptions) {
      log.info('Options are VALID');
      execStep(step, script, processing, name);
    } else {
      log.error('Options are NOT VALID');
    }
  } else {
    log.warn('Step deactivated. To activate change the step config from clean:false to clean:true. Be aware that following steps need the output from this step (e.g. from a previous run)');
    next();
  }
}

var execStep = function(step, script, processing, name) {
  script.process(step).then(function(result) {
    log.warn('here');
    result.skipped = !step.clean
    result.outputs = tools.getOutputs(step);
    processing.state = 'finished';

    var executionTime = time.timeEnd(name+' execution time');
    processing.executionTimeSeconds = executionTime;
    processing.endTime = Date.now();

    tools.setStepState(step.id, processing); 
    tools.setStepResult(step.id, result);

    if(result.msg) {
      log.output(result.msg);
    }
    next();
  }, errorHandling).catch(function(error) {
     log.error(error);
     var executionTime = time.timeEnd(name+' execution time');
     processing.state = 'failed';
     processing.endTime = Date.now();
     processing.executionTimeSeconds = executionTime;
     tools.setStepState(step.id, processing);
   });
}

var errorHandling = function(error) {
  log.error('process '+scriptPath+step.script+' failed because '+error);
  if(chain.stopOnFailure) {
    log.error('process '+scriptPath + step.script+' failed because '+error);
  } else {
    next();
  }
}

var next = function() {
  // continue with next step
  if(++i < chain.steps.length) {
    var options = chain.steps[i];
    options.number = i;
    options.outputDir = tools.getOutputDir(options);
    if(tools.fileExists(scriptPath+options.script+'.js')) {
      try {
        requireScript(options);
      } catch(error) {
        log.error(error);
      }
    } else {
      log.error('Script '+scriptPath+options.script+'.js does not exist. The id is referenced in config.');
    }
  } else {
    var totalExecutionTime = time.timeEnd('Total execution time');
    log.info(totalExecutionTime);
  }
}

var init = function() {
  // argument preprocessing.
  var argument = process.argv[2]
  var configFile = ''

  log.line();
  if(!argument) {
    log.error('Please provide the configuration file a first argument: "node process.js config.js"');
    return;
  }
  if(configFile.indexOf('config') === -1) {
    configFile = './config/'+argument;
  }
  if(configFile.indexOf('.js') === -1) {
    configFile += '.js';
  }

  if(!fs.existsSync(configFile)) {
    log.error('Could not find configuration file: '+argument);
  } else {
    log.debug('Configuration File: '+configFile);
    chain = require(configFile);
    var processing = tools.initProcess(chain);

    var validationResult = tools.validateConfig(chain.steps);
    // return validation results
    if(validationResult.length === 0) {
      scriptPath = chain.config.scriptPath;
      var options = chain.steps[0];

      options.number = 0;
      options.outputDir = tools.getOutputDir(options);
      requireScript(options);
    } else {
      log.error('Provided config is invalid. Please check '+configFile);
      for(var i=0; i < validationResult.length; i++) {
        if(validationResult[i].id) {
          log.debug(validationResult[i].id+': ');
        };
        for(var j=0; j < validationResult[i].validation.length; j++) {
          log.debug('- '+validationResult[i].validation[j]);
        }
      }
    }
  }
};

var doImport = function() {
  tools = require('./lib/tools');
  time = require('./lib/time');
  time.time('Total execution time');
  log = require('./lib/logger');
  log.setLogLevel('debug');
}

// Install npm packages if there is no mode_module folder
// logger is not available here: use console.log here instead of log.info/log.error ...
if(fs.existsSync('node_modules')) { 
  //console.log('found node_modules folder, everything installed');
  doImport();
  init();
} else {
  console.log('node_modules folder does not exist, run npm install');
  exec('npm install', {maxBuffer: 1024 * 500}, function(error, stdout, stderr) {
    if (error !== null) {
      console.log('exec error: ' + error);
    } else {
      console.log('npm install was successfull, run script');
      doImport();
      init();
    }
  });
}
