echo "Stop all Container"
docker stop $(docker ps -a -q)
echo ""
echo "Remove all Container"
docker rm $(docker ps -a -q)

echo ""
echo "----------------"
echo "Run docker image"
echo "----------------"

if [ $(docker images -q processing) ] ; then
  docker run --privileged -t -i --rm --name processing -v /var/run/docker.sock:/var/run/docker.sock -v "$PWD"/:/data -w /data -p 8080:80 processing ; node --max_old_space_size=4000 process.js resample
else
  echo "Need to build docker image: build_docker.sh"
  sh build_docker.sh
  sh run_with_docker.sh
fi
